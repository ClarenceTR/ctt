﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTTSBoard
{
    Dictionary<Vector2Int, CTTSPiece> m_board;

    public Dictionary<Vector2Int, CTTSPiece> Board => m_board;

    public CTTSPiece this[Vector2Int position]
    {
        get
        {
            // If this position is an available location, return the slot.
            // Else, return null.
            if (m_board.ContainsKey(position))
                return m_board[position];
            else
                return new CTTSPiece() { Type = 0xff, Faction = 0xff };
        }
        set
        {
            var piece = this[position];
            if (!piece.IsInvalid)
            {
                m_board[position] = value;
            }
            // If out of bound, nothing happens.
        }
    }

    public CTTSPiece this[int x, int y]
    {
        get
        {
            var v = new Vector2Int(x, y);
            return this[v];
        }
        set
        {
            var v = new Vector2Int(x, y);
            this[v] = value;
        }
    }

    public CTTSBoard()
    {
        m_board = new Dictionary<Vector2Int, CTTSPiece>();
    }

    public void Reset()
    {
        // Creates a board with inital setting.
        SetupEmpty();

        // y = x faction (Left):
        for (int x = 3; x <= 5; x++)
            for (int y = -x + 8; y <= 5; y++)
            {
                this[x, y] = new CTTSPiece(CTTSType.Pawn, CTTSFaction.One);   // Supporter
                this[-x, -y] = new CTTSPiece(CTTSType.Pawn, CTTSFaction.One);   // Fief
            }
        // (-5, -5) is the prince.
        this[-5, -5] = new CTTSPiece(CTTSType.Prince, CTTSFaction.One);
        this[-5, -3] = new CTTSPiece(CTTSType.Knight, CTTSFaction.One);
        this[-4, -4] = new CTTSPiece(CTTSType.Rook, CTTSFaction.One);
        this[-3, -5] = new CTTSPiece(CTTSType.Knight, CTTSFaction.One);

        // y = -x faction (Right):
        for (int x = -10; x <= -8; x++)
            for (int y = -x - 5; y <= 5; y++)
            {
                this[x, y] = new CTTSPiece(CTTSType.Pawn, CTTSFaction.Two);   // Supporter
                this[-x, -y] = new CTTSPiece(CTTSType.Pawn, CTTSFaction.Two);   // Fief
            }
        // (10, -5) is the prince.
        this[10, -5] = new CTTSPiece(CTTSType.Prince, CTTSFaction.Two);
        this[8, -3] = new CTTSPiece(CTTSType.Knight, CTTSFaction.Two);
        this[8, -4] = new CTTSPiece(CTTSType.Rook, CTTSFaction.Two);
        this[8, -5] = new CTTSPiece(CTTSType.Knight, CTTSFaction.Two);

        // Debug:
        //this[-1,-5] = new CTTSPiece(CTTSType.Prince, CTTSFaction.Two);    // Test prince captured winning condition.
        //this[-6, 11] = new CTTSPiece(CTTSType.Prince, CTTSFaction.One);   // Test prince reach winning condition.
        this[-0, -5] = new CTTSPiece(CTTSType.Pawn, CTTSFaction.Two);
        this[-2, -5] = new CTTSPiece(CTTSType.Pawn, CTTSFaction.Two);
        this[-4, -3] = new CTTSPiece(CTTSType.Pawn, CTTSFaction.Two);
        this[-4, -2] = new CTTSPiece(CTTSType.Pawn, CTTSFaction.Two);
        this[-3, -4] = new CTTSPiece(CTTSType.Pawn, CTTSFaction.One);
        this[-2, -4] = new CTTSPiece(CTTSType.Pawn, CTTSFaction.One);
    }

    public void SetupEmpty()
    {
        m_board.Clear();

        // Create a hexagon of radius 8.
        int b = 8 - 1;
        for (int x = -b; x <= b; x++)
        {
            for (int y = Mathf.Max(-x - b, -b); y <= Mathf.Min(-x + b, b); y++)
            {
                m_board.Add(new Vector2Int(x, y), CTTSPiece.Empty);
            }
        }

        // Add capital triangle:
        for (int x = -6; x <= -2; x++)
            for (int y = 8; y <= -x + 6; y++)
            {
                m_board.Add(new Vector2Int(x, y), CTTSPiece.Empty);
            }

        // Add the y=-x faction (Right):
        for (int x = -10; x <= -8; x++)
            for (int y = -x - 5; y <= 5; y++)
            {
                // The supporter's area for right
                m_board.Add(new Vector2Int(x, y), CTTSPiece.Empty);

                // The fief for right
                m_board.Add(new Vector2Int(-x, -y), CTTSPiece.Empty);
            }

        // Add the y = x faction:
        for (int x = 3; x <= 5; x++)
            for (int y = -x + 8; y <= 5; y++)
            {
                // The supporter's area for left
                m_board.Add(new Vector2Int(x, y), CTTSPiece.Empty);

                // The fief for left
                m_board.Add(new Vector2Int(-x, -y), CTTSPiece.Empty);
            }
    }

    public CTTSBoard Copy
    {
        get
        {
            CTTSBoard copy = new CTTSBoard();
            foreach (var pos in this.m_board.Keys)
            {
                copy.m_board.Add(pos, this[pos]);
            }
            return copy;
        }
    }
}