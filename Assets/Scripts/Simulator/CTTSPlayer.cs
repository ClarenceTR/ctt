﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTTSPlayer
{
    public bool EndTurn;
    public byte Faction { get => m_faction; set => m_faction = value; }

    byte m_faction = CTTSFaction.None;

    CTTSBoard m_realBoard;
    CTTSBoard m_mockBoard = null;
    bool m_mockBoardIsDirty = true;

    CTTSPiece m_selectedPiece = CTTSPiece.Null;
    Vector2Int m_selectedPos;
    public bool Selected => !m_selectedPiece.IsInvalid && !m_selectedPiece.IsEmpty;
    public Vector2Int SelectedPosition => m_selectedPos;
    public CTTSPiece SelectedPiece => m_selectedPiece;

    CTTSMove m_currentMove;
    public bool Moved => m_currentMove.Count > 0;

    Dictionary<Vector2Int, CTTSAvailableMove> m_currAvailableMoves = new Dictionary<Vector2Int, CTTSAvailableMove>();

    public CTTSBoard MockBoard => m_mockBoard;
    public bool MockBoardDirty => m_mockBoardIsDirty;

    public CTTSPlayer(CTTSBoard board, byte faction)
    {
        m_realBoard = board;
        m_faction = faction;
    }

    /// <summary>
    /// If the mock board is dirty, copy. Otherwise, use the mock board.
    /// </summary>
    public void ResetMockBoard()
    {
        if (m_mockBoardIsDirty)
        {
            m_mockBoard = m_realBoard.Copy;
            m_mockBoardIsDirty = false;
        }
    }

    public void SelectNothing()
    {
        m_selectedPiece = CTTSPiece.Null;
        m_selectedPos = new Vector2Int();
    }

    public bool Select(Vector2Int position)
    {
        var p = m_realBoard[position];
        if (p.IsInvalid || p.IsEmpty) return false;

        // Can't select pieces of other factions.
        if (p.Faction != m_faction) return false;

        // Can select.
        m_selectedPiece = p;
        m_selectedPos = position;
        // Copy to the mock Board.
        ResetMockBoard();

        // Start the record of move.
        m_currentMove = new CTTSMove()
        {
            Faction = this.m_faction,
            PieceType = m_selectedPiece.Type,
        };

        return true;
    }

    public Dictionary<Vector2Int, CTTSAvailableMove> CalculateAvailable()
    {
        // Assume the selectedPawn is selected.
        Debug.Assert(Selected);

        m_currAvailableMoves = CTTSLogic.CalculateAvailable(m_selectedPos, m_mockBoard, m_currentMove);
        return m_currAvailableMoves;
    }

    //Stack<CTTAvailableMove> m_mockedMoves = new Stack<CTTAvailableMove>();

    /// <summary>
    /// Go to the position among the available positions from CalculateAvailable()
    /// </summary>
    public void MockMove(Vector2Int position)
    {
        // Assume the selectedPawn is selected.
        Debug.Assert(Selected);
        // Assume the position is in the m_currAvailableMoves
        Debug.Assert(m_currAvailableMoves.ContainsKey(position));

        // Go to this position on the mock board.
        // Get that available move.
        var move = m_currAvailableMoves[position];
        // Go to the end position on the mock board.
        m_mockBoard[m_selectedPos] = CTTSPiece.Empty;
        m_mockBoard[m_selectedPos = move.EndPosition] = m_selectedPiece;
        // Remove the pieces on the mock board.
        foreach (var p in move.PiecesRemoved.Keys)
        {
            m_mockBoard[p] = CTTSPiece.Empty;
        }
        // Label as dirty.
        m_mockBoardIsDirty = true;

        // Add to the move record.
        m_currentMove.AddMove(move);
    }

    public void UndoMockMove()
    {
        // Assume the selectedPawn is selected.
        Debug.Assert(Selected);

        // Can't undo when no mocked moves.
        if (m_currentMove.Count == 0) return;

        var move = m_currentMove.LastMove;
        if (m_currentMove.UndoMove())
        {
            // Put the selected piece back.
            Debug.Assert(move.EndPosition == m_selectedPos);
            m_mockBoard[m_selectedPos] = CTTSPiece.Empty;
            m_mockBoard[m_selectedPos = move.StartPosition] = m_selectedPiece;

            // Put the captured pieces back.
            foreach (var p in move.PiecesRemoved)
            {
                m_mockBoard[p.Key] = p.Value;
            }
            // Remain the same dirtyness status. 
        }
        // Won't succeed if there's no move.
    }

    /// <summary>
    /// Top is the first move. bottom is the last move.
    /// </summary>
    public Stack<CTTSAvailableMove> MoveStack
    {
        get
        {
            var s = new Stack<CTTSAvailableMove>();
            for (int i = m_currentMove.moves.Count - 1; i >= 0; i--)
                s.Push(m_currentMove.moves[i]);
            return s;
        }
    }
}
