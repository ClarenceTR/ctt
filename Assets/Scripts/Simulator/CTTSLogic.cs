﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

static public class CTTSLogic
{
    public static Dictionary<Vector2Int, CTTSAvailableMove> CalculateAvailable(Vector2Int position, CTTSBoard board, CTTSMove currentMove)
    {
        Dictionary<Vector2Int, CTTSAvailableMove> res = new Dictionary<Vector2Int, CTTSAvailableMove>();

        var piece = board[position];
        // Empty or out of bound renders this action invalid. No available moves.
        if (piece.IsEmpty || piece.IsInvalid) return res;

        // If charged during the current move, can only go the certain direction.


        // If stepped, unable to do anything else.
        if (currentMove.MoveInfo.Stepped) return res;

        Vector2Int
            right1 = position + Vector2Int.right,
            right2 = position + new Vector2Int(2, 0),
            upRight1 = position + Vector2Int.up,
            upRight2 = position + new Vector2Int(0, 2),
            upLeft1 = position + new Vector2Int(-1, 1),
            upLeft2 = position + new Vector2Int(-2, 2),
            left1 = position + Vector2Int.left,
            left2 = position + new Vector2Int(-2, 0),
            downLeft1 = position + Vector2Int.down,
            downLeft2 = position + new Vector2Int(0, -2),
            downRight1 = position + new Vector2Int(1, -1),
            downRight2 = position + new Vector2Int(2, -2);
        CTTSPiece
            right1Piece = board[right1],
            right2Piece = board[right2],
            upRight1Piece = board[upRight1],
            upRight2Piece = board[upRight2],
            upLeft1Piece = board[upLeft1],
            upLeft2Piece = board[upLeft2],
            left1Piece = board[left1],
            left2Piece = board[left2],
            downLeft1Piece = board[downLeft1],
            downLeft2Piece = board[downLeft2],
            downRight1Piece = board[downRight1],
            downRight2Piece = board[downRight2];

        CTTSAvailableMove move;
        // Check step.
        // If jumped, can't step.
        if (!currentMove.MoveInfo.AnyJumped)
        {
            if (TryStep(ref position, ref right1, ref piece, ref right1Piece, out move)) res.Add(move.EndPosition, move);
            if (TryStep(ref position, ref upRight1, ref piece, ref upRight1Piece, out move)) res.Add(move.EndPosition, move);
            if (TryStep(ref position, ref upLeft1, ref piece, ref upLeft1Piece, out move)) res.Add(move.EndPosition, move);
            if (TryStep(ref position, ref left1, ref piece, ref left1Piece, out move)) res.Add(move.EndPosition, move);
            if (TryStep(ref position, ref downLeft1, ref piece, ref downLeft1Piece, out move)) res.Add(move.EndPosition, move);
            if (TryStep(ref position, ref downRight1, ref piece, ref downRight1Piece, out move)) res.Add(move.EndPosition, move);
        }

        bool jumpCheck = true;
        // Check charge.
        //If it's a knight.
        if (piece.Type == CTTSType.Knight)
        {
            // If it has not charged but jumped, can both charge and jump.
            bool jumped = currentMove.MoveInfo.Jumped;
            bool charged = currentMove.MoveInfo.Charged;
            if (jumped && !charged)
            {
                // Get last move's direction.
                var lm = currentMove.LastMove;
                Debug.Assert(lm != null);
                CTTSDirection dir = GetDirection(ref lm.StartPosition, ref lm.EndPosition);

                // Add the charge (a special "step") available position 
                switch (dir)
                {
                    case CTTSDirection.Right:
                        if (TryStep(ref position, ref right1, ref piece, ref right1Piece, out move)) { move.MoveType = CTTSMoveType.Charge; res.Add(move.EndPosition, move); };
                        break;
                    case CTTSDirection.Upright:
                        if (TryStep(ref position, ref upRight1, ref piece, ref upRight1Piece, out move)) { move.MoveType = CTTSMoveType.Charge; res.Add(move.EndPosition, move); };
                        break;
                    case CTTSDirection.Upleft:
                        if (TryStep(ref position, ref upLeft1, ref piece, ref upLeft1Piece, out move)) { move.MoveType = CTTSMoveType.Charge; res.Add(move.EndPosition, move); };
                        break;
                    case CTTSDirection.Left:
                        if (TryStep(ref position, ref left1, ref piece, ref left1Piece, out move)) { move.MoveType = CTTSMoveType.Charge; res.Add(move.EndPosition, move); };
                        break;
                    case CTTSDirection.Downleft:
                        if (TryStep(ref position, ref downLeft1, ref piece, ref downLeft1Piece, out move)) { move.MoveType = CTTSMoveType.Charge; res.Add(move.EndPosition, move); };
                        break;
                    case CTTSDirection.Downright:
                        if (TryStep(ref position, ref downRight1, ref piece, ref downRight1Piece, out move)) { move.MoveType = CTTSMoveType.Charge; res.Add(move.EndPosition, move); };
                        break;
                    case CTTSDirection.Invalid:
                        break;
                    default:
                        break;
                }

                // Still can jump.
            }
            // Else if it has both jumped and charged, can jump in the same direction.
            else if (jumped && charged)
            {
                Debug.Assert(currentMove.ChargeDirection != CTTSDirection.Invalid);
                switch (currentMove.ChargeDirection)
                {
                    case CTTSDirection.Right:
                        if (TryJump(ref position, ref right1, ref right2, ref piece, ref right1Piece, ref right2Piece, out move)) res.Add(move.EndPosition, move);
                        break;
                    case CTTSDirection.Upright:
                        if (TryJump(ref position, ref upRight1, ref upRight2, ref piece, ref upRight1Piece, ref upRight2Piece, out move)) res.Add(move.EndPosition, move);
                        break;
                    case CTTSDirection.Upleft:
                        if (TryJump(ref position, ref upLeft1, ref upLeft2, ref piece, ref upLeft1Piece, ref upLeft2Piece, out move)) res.Add(move.EndPosition, move);
                        break;
                    case CTTSDirection.Left:
                        if (TryJump(ref position, ref left1, ref left2, ref piece, ref left1Piece, ref left2Piece, out move)) res.Add(move.EndPosition, move);
                        break;
                    case CTTSDirection.Downleft:
                        if (TryJump(ref position, ref downLeft1, ref downLeft2, ref piece, ref downLeft1Piece, ref downLeft2Piece, out move)) res.Add(move.EndPosition, move);
                        break;
                    case CTTSDirection.Downright:
                        if (TryJump(ref position, ref downRight1, ref downRight2, ref piece, ref downRight1Piece, ref downRight2Piece, out move)) res.Add(move.EndPosition, move);
                        break;
                    case CTTSDirection.Invalid:
                        break;
                    default:
                        break;
                }

                // Jump is done. Don't goto jump logic.
                jumpCheck = false;
            }
            // Else it's impossible to have charged but not jumped.
            // Else, it's the same as the rest of the pieces.
            else
            {
                Debug.Assert(!(charged && !jumped));
                // Still can jump.
            }
        }

        // If it's a rook. Don't check jump. Big jump will take care of the checking.
        if (piece.Type == CTTSType.Rook)
            jumpCheck = false;

        if (jumpCheck)
        {
            // Can jump in all directions.
            if (TryJump(ref position, ref right1, ref right2, ref piece, ref right1Piece, ref right2Piece, out move)) res.Add(move.EndPosition, move);
            if (TryJump(ref position, ref upRight1, ref upRight2, ref piece, ref upRight1Piece, ref upRight2Piece, out move)) res.Add(move.EndPosition, move);
            if (TryJump(ref position, ref upLeft1, ref upLeft2, ref piece, ref upLeft1Piece, ref upLeft2Piece, out move)) res.Add(move.EndPosition, move);
            if (TryJump(ref position, ref left1, ref left2, ref piece, ref left1Piece, ref left2Piece, out move)) res.Add(move.EndPosition, move);
            if (TryJump(ref position, ref downLeft1, ref downLeft2, ref piece, ref downLeft1Piece, ref downLeft2Piece, out move)) res.Add(move.EndPosition, move);
            if (TryJump(ref position, ref downRight1, ref downRight2, ref piece, ref downRight1Piece, ref downRight2Piece, out move)) res.Add(move.EndPosition, move);
        }

        if (piece.Type == CTTSType.Rook)
        {
            // It also needs to check big steps.
            Vector2Int
                  right3 = position + new Vector2Int(3, 0),
                upRight3 = position + new Vector2Int(0, 3),
                 upLeft3 = position + new Vector2Int(-3, 3),
                   left3 = position + new Vector2Int(-3, 0),
               downLeft3 = position + new Vector2Int(0, -3),
              downRight3 = position + new Vector2Int(3, -3);
            CTTSPiece
                right3Piece = board[right3],
                upRight3Piece = board[upRight3],
                upLeft3Piece = board[upLeft3],
                left3Piece = board[left3],
                downLeft3Piece = board[downLeft3],
                downRight3Piece = board[downRight3];

            if (TryBigJump(ref position, ref right1, ref right2, ref right3, ref piece, ref right1Piece, ref right2Piece, ref right3Piece, out move)) res.Add(move.EndPosition, move);
            if (TryBigJump(ref position, ref upRight1, ref upRight2, ref upRight3, ref piece, ref upRight1Piece, ref upRight2Piece, ref upRight3Piece, out move)) res.Add(move.EndPosition, move);
            if (TryBigJump(ref position, ref upLeft1, ref upLeft2, ref upLeft3, ref piece, ref upLeft1Piece, ref upLeft2Piece, ref upLeft3Piece, out move)) res.Add(move.EndPosition, move);
            if (TryBigJump(ref position, ref left1, ref left2, ref left3, ref piece, ref left1Piece, ref left2Piece, ref left3Piece, out move)) res.Add(move.EndPosition, move);
            if (TryBigJump(ref position, ref downLeft1, ref downLeft2, ref downLeft3, ref piece, ref downLeft1Piece, ref downLeft2Piece, ref downLeft3Piece, out move)) res.Add(move.EndPosition, move);
            if (TryBigJump(ref position, ref downRight1, ref downRight2, ref downRight3, ref piece, ref downRight1Piece, ref downRight2Piece, ref downRight3Piece, out move)) res.Add(move.EndPosition, move);
        }
        return res;
    }

    static bool TryJump(ref Vector2Int me, ref Vector2Int mid, ref Vector2Int dest, ref CTTSPiece meP, ref CTTSPiece midP, ref CTTSPiece destP,
          out CTTSAvailableMove move)
    {
        move = CTTSAvailableMove.Invalid;
        // if position next to is empty or out of bound, can't jump.
        if (midP.IsEmpty || midP.IsInvalid) return false;
        // if the destination is not empty or out of bound, can't jump.
        // TODO: out of bound can jump for the elite pieces, and is just temporarily removed.
        if (!destP.IsEmpty) return false;

        // Same faction doesn't capture.
        if (meP.Faction != midP.Faction)
        {
            // Not same. Capture.
            move.SetValidation(true);
            move.PiecesRemoved.Add(mid, midP);
        }
        // Either way, start/end position is the same.
        move.StartPosition = me;
        move.EndPosition = dest;
        move.MoveType = CTTSMoveType.Jump;
        return true;
    }

    static bool TryStep(ref Vector2Int me, ref Vector2Int dest, ref CTTSPiece meP, ref CTTSPiece destP,
       out CTTSAvailableMove move)
    {
        move = CTTSAvailableMove.Invalid;
        // if position next to is not empty or out of bound, can't step.
        if (!destP.IsEmpty) return false;

        // Won't capture anything.
        move.SetValidation(true);
        move.StartPosition = me;
        move.EndPosition = dest;
        move.MoveType = CTTSMoveType.Step;
        return true;
    }

    static bool TryBigJump(ref Vector2Int me, ref Vector2Int mid1, ref Vector2Int mid2, ref Vector2Int dest, ref CTTSPiece meP, ref CTTSPiece mid1P, ref CTTSPiece mid2P, ref CTTSPiece destP,
        out CTTSAvailableMove move)
    {
        move = CTTSAvailableMove.Invalid;

        // if mid2 is empty or out of bound, treat as TryJump.
        if (mid2P.IsEmpty || mid2P.IsInvalid)
            return TryJump(ref me, ref mid1, ref mid2, ref meP, ref mid1P, ref mid2P, out move);

        // if mid1 is empty or out of bound, can't jump.
        // Possible optimization (less flexible though): in current map, mid1P won't be and shouldn't be invalid if mid2 is neither empty or our of bound.
        if (mid1P.IsEmpty || mid1P.IsInvalid) return false;
        // if the destination is not empty or out of bound, can't jump.
        // TODO: out of bound can jump for the elite pieces, and is just temporarily removed.
        if (!destP.IsEmpty) return false;

        // Same faction doesn't capture.
        if (meP.Faction != mid1P.Faction)
        {
            // Not same. Capture.
            move.SetValidation(true);
            move.PiecesRemoved.Add(mid1, mid1P);
        }
        if (meP.Faction != mid2P.Faction)
        {
            // Not same. Capture.
            move.SetValidation(true);
            move.PiecesRemoved.Add(mid2, mid2P);
        }

        // Anyway, start/end position is dest.
        move.StartPosition = me;
        move.EndPosition = dest;
        move.MoveType = CTTSMoveType.BigJump;
        return true;
    }

    static public CTTSDirection GetDirection(ref Vector2Int start, ref Vector2Int end)
    {
        // Three axes: y=-x + c; x=c; y=c;
        // Thus, three slopes: -1, Infinite and 0.
        int k;
        if (end.x - start.x == 0)
        {
            // x=c.
            if (end.y > start.y) return CTTSDirection.Upright;
            else if (end.y < start.y) return CTTSDirection.Downleft;
            else /* Same point */return CTTSDirection.Invalid;
        }
        else
        {
            k = (end.y - start.y) / (end.x - start.x);
            if (k == 0)
            {
                // y=c
                if (end.x > start.x) return CTTSDirection.Right;
                return CTTSDirection.Left;
                // At this point, same point should have already been filtered.
            }
            else if (k == -1)
            {
                // y = -x + c
                if (end.x > start.x) return CTTSDirection.Downright;
                return CTTSDirection.Upleft;
                // At this point, same point should have already been filtered.
            }
            else
            {
                // Invalid.
                return CTTSDirection.Invalid;
            }
        }

    }
}
