﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public struct CTTSPiece
{
    public byte Type;    // Default: Empty
    public byte Faction;        // Default: No faction

    public static CTTSPiece Empty { get => new CTTSPiece(CTTSType.Empty, CTTSFaction.None); }
    public static CTTSPiece Null { get => new CTTSPiece(CTTSType.Null, CTTSFaction.None); }

    public CTTSPiece(byte type, byte faction)
    {
        this.Type = type;
        this.Faction = faction;
    }

    /// <summary>
    /// If it is Piece.Null
    /// </summary>
    public bool IsInvalid => (Type ^ 0xff) == 0;

    /// <summary>
    /// If it is Piece.Empty
    /// </summary>
    public bool IsEmpty => (Type ^ 0x00) == 0;
}

static class CTTSType
{
    public const byte Empty = 0x00;
    public const byte Null = 0xff;
    public const byte Pawn = 0x01;
    public const byte Knight = 0x02;
    public const byte Rook = 0x04;
    public const byte Prince = 0x08;

    public static string ToName(byte type)
    {
        switch (type)
        {
            case 0x00:
                return "Empty";
            case 0xff:
                return "Invalid";
            case 0x01:
                return "Pawn";
            case 0x02:
                return "Knight";
            case 0x04:
                return "Rook";
            case 0x08:
                return "Prince";
            default:
                return string.Empty;
        }
    }
}

static class CTTSFaction
{
    public const byte Player = 0x00;
    public const byte One = 0x01;
    public const byte Two = 0x02;
    public const byte None = 0xff;

    public static bool IsFaction(byte faction)
    {
        return faction != None;
    }
}

public class CTTSMove
{
    public byte Faction;
    public byte PieceType;
    public List<CTTSAvailableMove> moves = new List<CTTSAvailableMove>();
    public Vector2Int? StartPosition
    {
        get
        {
            if (moves.Count > 0) return moves[0].StartPosition;
            else return null;
        }
    }
    public Stack<Vector2Int> EndPositionSequence
    {
        // Top: first position; Bottom: Last position
        get
        {
            var endPoses = new Stack<Vector2Int>();
            for (int i = moves.Count - 1; i >= 0; i--)
            {
                endPoses.Push(moves[i].EndPosition);
            }
            return endPoses;
        }
    }
    public CTTSDirection ChargeDirection = CTTSDirection.Invalid;
    public CTTSMoveInfo MoveInfo = new CTTSMoveInfo();

    public CTTSAvailableMove LastMove => moves.Count > 0 ? moves.Last() : null;
    public int Count => moves.Count;

    public void AddMove(CTTSAvailableMove move)
    {
        // ? Is it necessary to ask if start position == last time end position?

        // Record this move.
        moves.Add(move);

        UpdateMoveInfo();

        // If it's a charge, record charge direction.
        if (move.MoveType == CTTSMoveType.Charge)
        {
            ChargeDirection = CTTSLogic.GetDirection(ref move.StartPosition, ref move.EndPosition);
        }
    }

    public bool UndoMove()
    {
        // Can't undo when no moves done.
        if (moves.Count == 0) return false;

        // Get the last move.
        var lastMove = moves.Last();

        // Remove this move from the mocked move stack.
        moves.Remove(lastMove);

        // Update move info and related.
        // Recalculate the moveInfo.
        UpdateMoveInfo();

        // Reset charge direction if nolonger charged.
        if (!MoveInfo.Charged) ChargeDirection = CTTSDirection.Invalid;

        return true;
    }

    void UpdateMoveInfo()
    {
        byte mi = 0x00;
        foreach (var m in moves)
            mi |= m.MoveType;
        MoveInfo.Info = new BitArray(new byte[] { mi });
    }

    //public bool IsInvalid() => (PieceType ^ 0xff) == 0;
}

public class CTTSAvailableMove
{
    bool m_invalid;
    public Vector2Int StartPosition;
    public Vector2Int EndPosition;
    public Dictionary<Vector2Int, CTTSPiece> PiecesRemoved;
    public byte MoveType;

    public CTTSAvailableMove()
    {
        m_invalid = false;
        PiecesRemoved = new Dictionary<Vector2Int, CTTSPiece>();
    }

    public static CTTSAvailableMove Invalid
    {
        get
        {
            return new CTTSAvailableMove() { m_invalid = true, };
        }
    }

    public bool IsInvalid() => m_invalid == true;
    public void SetValidation(bool value) { m_invalid = !value; if (m_invalid) { EndPosition = new Vector2Int(); PiecesRemoved.Clear(); } }
}

static class CTTSMoveType
{
    public const byte Step = 0x01;
    public const byte Jump = 0x02;
    public const byte Charge = 0x04; // A special step
    public const byte BigJump = 0x08;
}

public class CTTSMoveInfo
{
    public BitArray Info = new BitArray(8, false); // [0] is the digit in ones place, [1] is the digit in tens place, ...

    public bool Stepped
    {
        get
        {
            return Info[0];
        }
    }

    public bool Jumped
    {
        get
        {
            return Info[1];
        }
    }

    public bool Charged
    {
        get
        {
            return Info[2];
        }
    }

    public bool BigJumped
    {
        get
        {
            return Info[3];
        }
    }

    public bool AnyJumped
    {
        get
        {
            return Info[1] || Info[3];
        }
    }
}

public enum CTTSDirection
{
    Right,
    Upright,
    Upleft,
    Left,
    Downleft,
    Downright,
    Invalid,
}
