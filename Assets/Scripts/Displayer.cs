﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[ExecuteInEditMode()]
public class Displayer : MonoBehaviour
{
    public LayerMask nodeLayer;
    public TextMeshProUGUI text;
    public TextMeshProUGUI stateText;

    private void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, float.MaxValue, nodeLayer))
        {
            var cell = hit.collider.GetComponent<Cell>();
            text.text = (cell.OffsetCoord).ToString();
        }

        var state = CTTGameManagerOld.Instance?.GameState;
        stateText.text = state?.GetType().ToString();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(Camera.main.ScreenPointToRay(Input.mousePosition));
    }
}
