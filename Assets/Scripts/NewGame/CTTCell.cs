﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CTTCell : MonoBehaviour
{
    [SerializeField]
    Vector2Int m_coord;

    private CTTCellManager mr_manager;

    private Renderer hexagonRenderer;
    private Renderer outlineRenderer;
    private Color defaultHexagonColor;
    private Color defaultOutlineColor;

    public void Awake()
    {
        mr_manager = GetComponentInParent<CTTCellManager>();

        hexagonRenderer = GetComponent<Renderer>();

        var outline = transform.Find("Outline");
        outlineRenderer = outline.GetComponent<Renderer>();

        defaultHexagonColor = GetColor(hexagonRenderer);
        defaultOutlineColor = GetColor(outlineRenderer);
        SetColor(hexagonRenderer, defaultHexagonColor);
        SetColor(outlineRenderer, defaultOutlineColor);
    }

    public Vector2Int Coord { get => m_coord; set => m_coord = value; }

    public Vector3 GetCellDimensions()
    {
        var outline = transform.Find("Outline");
        var outlineRenderer = outline.GetComponent<Renderer>();
        var size = outlineRenderer.bounds.size;
        return size;
    }

    public void MarkAsHighlighted()
    {
        SetColor(hexagonRenderer, Color.cyan);
        SetColor(outlineRenderer, Color.yellow);
    }
    public void MarkAsFriendly()
    {
        SetColor(hexagonRenderer, Color.green);
        SetColor(outlineRenderer, Color.yellow);
    }
    public void MarkAsHostile()
    {
        SetColor(hexagonRenderer, Color.red);
        SetColor(outlineRenderer, Color.yellow);
    }
    public void MarkAsIndicated()
    {
        // Blink.
        //StartCoroutine(C_BlinkColor(defaultHexagonColor, Color.cyan, 1f));
        if (c_BlinkColor != null) StopCoroutine(c_BlinkColor);
        c_BlinkColor = StartCoroutine(C_BlinkColor(defaultHexagonColor, Color.cyan, 2f));
    }
    public void MarkAsReachable()
    {
        SetColor(hexagonRenderer, Color.green);
        SetColor(outlineRenderer, Color.yellow);
    }
    public void MarkAsUnreachable()
    {
        SetColor(hexagonRenderer, Color.red);
        SetColor(outlineRenderer, Color.yellow);
    }
    public void MarkAsStartPoint()
    {
        SetColor(hexagonRenderer, Color.yellow);
        SetColor(outlineRenderer, Color.yellow);
    }
    public void MarkAsPath()
    {
        SetColor(hexagonRenderer, Color.cyan);
    }

    public void UnMark()
    {
        SetColor(hexagonRenderer, defaultHexagonColor);
        SetColor(outlineRenderer, defaultOutlineColor);
    }

    public void UnMarkTotally()
    {
        UnMark();

        // Also cancel the blinking.
        //c_blinkColorContinuing = false;
        //DOTween.Kill(t_blink);
        if (c_BlinkColor != null) StopCoroutine(c_BlinkColor);
    }

    #region Helpers

    private void SetColor(Renderer renderer, Color color)
    {
        renderer.material.color = color;
    }

    private Color GetColor(Renderer renderer)
    {
        return renderer.material.color;
    }

    //private Sequence t_blink;
    //private void BlinkColor(Color color1, Color color2, float duration)
    //{
    //    t_blink = DOTween.Sequence();
    //    t_blink.Append(hexagonRenderer.material.DOColor(color2, duration));
    //    t_blink.Append(hexagonRenderer.material.DOColor(color1, duration));
    //    t_blink.SetLoops(short.MaxValue, LoopType.Restart);
    //}

    //private bool c_blinkColorContinuing = false;
    private Coroutine c_BlinkColor = null;
    private IEnumerator C_BlinkColor(Color color1, Color color2, float duration)
    {
        while (true)
        {
            float lerpTime = 0;
            while (true)
            {
                lerpTime += Time.deltaTime;
                float perc = lerpTime / duration;
                if (perc > 1) break;
                hexagonRenderer.material.color = Color.Lerp(color1, color2, mr_manager.BlinkCurve.Evaluate(perc));
                yield return new WaitForEndOfFrame();
            }
            lerpTime = 0;
            while (true)
            {
                lerpTime += Time.deltaTime;
                float perc = lerpTime / duration;
                if (perc > 1) break;
                hexagonRenderer.material.color = Color.Lerp(color2, color1, mr_manager.BlinkCurve.Evaluate(perc));
                yield return new WaitForEndOfFrame();
            }
        }
    }

    #endregion

    #region Events

    /// <summary>
    /// CellClicked event is invoked when user clicks on the cell. 
    /// It requires a collider on the cell game object to work.
    /// </summary>
    public event EventHandler CellClicked;
    /// <summary>
    /// CellHighlighed event is invoked when cursor enters the cell's collider. 
    /// It requires a collider on the cell game object to work.
    /// </summary>
    public event EventHandler CellHighlighted;
    /// <summary>
    /// CellDehighlighted event is invoked when cursor exits the cell's collider. 
    /// It requires a collider on the cell game object to work.
    /// </summary>
    public event EventHandler CellDehighlighted;

    #endregion

    #region Input Detection

    protected virtual void OnMouseEnter()
    {
        if (CellHighlighted != null)
            CellHighlighted.Invoke(this, new EventArgs());
    }
    protected virtual void OnMouseExit()
    {
        if (CellDehighlighted != null)
            CellDehighlighted.Invoke(this, new EventArgs());
    }
    void OnMouseDown()
    {
        if (CellClicked != null)
            CellClicked.Invoke(this, new EventArgs());
    }

    #endregion

    #region Equals override

    public bool Equals(CTTCell other)
    {
        return (m_coord.x == other.m_coord.x && m_coord.y == other.m_coord.y);
    }

    public override bool Equals(object other)
    {
        if (!(other is CTTCell))
            return false;

        return Equals(other as CTTCell);
    }

    public override int GetHashCode()
    {
        int hash = 23;

        hash = (hash * 37) + Coord.x;
        hash = (hash * 37) + Coord.y;
        return hash;

    }

    #endregion

}
