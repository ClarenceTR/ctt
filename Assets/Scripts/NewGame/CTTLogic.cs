﻿using UnityEngine;

class CTTLogic
{
    //public static Quaternion DetermineOrientation(byte faction, Vector2Int position)
    //{
    //    if (faction == CTTSFaction.One)
    //    {
    //        if (position.x < 0)
    //        {
    //            // Fief side. Face upright.
    //            return CTTPieceOrientation.Upright;
    //        }
    //        else
    //        {
    //            // Face downleft.
    //            return CTTPieceOrientation.Downleft;
    //        }
    //    }
    //    else
    //    {
    //        if (position.x > 0)
    //        {
    //            // Fief side. Face upleft.
    //            return CTTPieceOrientation.Upleft;
    //        }
    //        else
    //        {
    //            // Face downright.
    //            return CTTPieceOrientation.Downright;
    //        }
    //    }
    //}

    public static CTTSDirection DetermineOrientation(byte faction, Vector2Int position)
    {
        if (faction == CTTSFaction.One)
        {
            if (position.x < 0)
            {
                // Fief side. Face upright.
                return CTTSDirection.Upright;
            }
            else
            {
                // Face downleft.
                return CTTSDirection.Downleft;
            }
        }
        else
        {
            if (position.x > 0)
            {
                // Fief side. Face upleft.
                return CTTSDirection.Upleft;
            }
            else
            {
                // Face downright.
                return CTTSDirection.Downright;
            }
        }
    }

    public static Vector3 LogicToViewPosition(int x, int y, ref Vector3 hexSize)
    {
        var pos = new Vector3(x * hexSize.x + y * hexSize.x * 0.5f, 0, y * hexSize.z * 0.75f);
        return pos;
    }

    public static byte GetFaction(int playerNumber)
    {
        switch (playerNumber)
        {
            case 0:
                return CTTSFaction.One;
            case 1:
                return CTTSFaction.Two;
            default:
                return CTTSFaction.None;
        }
    }
}
