﻿using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class CTTGameState
{
    protected CTTGameManager m_gm;

    protected CTTGameState(CTTGameManager gameManager)
    {
        m_gm = gameManager;
    }

    public virtual byte GetFaction(Vector2Int position, in CTTSBoard originalBoard, in CTTSBoard mockBoard)
    {
        // Tell which faction it is from the original board.
        return originalBoard[position].Faction;
    }

    public virtual void OnUnitHighlighted(CTTUnit unit)
    {
        Highlight(unit);
    }


    public virtual void OnUnitDehighlighted(CTTUnit unit)
    {
        Dehighlight(unit);
    }


    /// <summary>
    /// Method is called when a unit is clicked on.
    /// </summary>
    /// <param name="unit">Unit that was clicked.</param>
    public virtual void OnUnitClicked(CTTUnit unit)
    {
        // Let children determine.
    }

    /// <summary>
    /// Method is called when mouse enters cell's collider.
    /// </summary>
    /// <param name="cell">Cell that was selected.</param>
    public virtual void OnCellHighlighted(CTTCell cell)
    {
        var unit = m_gm.GetUnit(cell.Coord);
        if (unit) { Highlight(unit); }
        else { Highlight(cell); }
    }

    /// <summary>
    /// Method is called when mouse exits cell's collider.
    /// </summary>
    /// <param name="cell">Cell that was deselected.</param>
    public virtual void OnCellDehighlighted(CTTCell cell)
    {
        var unit = m_gm.GetUnit(cell.Coord);
        if (unit) { Dehighlight(unit); }
        else { Dehighlight(cell); }
    }

    /// <summary>
    /// Method is called when a cell is clicked.
    /// </summary>
    /// <param name="cell">Cell that was clicked.</param>
    public virtual void OnCellSelected(CTTCell cell)
    {
        // Let children determine.
    }

    public virtual void OnActionUndone()
    {
        // Let children determine.
    }

    public virtual void OnActionCancelled()
    {
        // Let children determine.
    }

    public virtual void OnActionConfirmed()
    {
        // Let children determine.
    }

    /// <summary>
    /// Method is called on transitioning into a state.
    /// </summary>
    public virtual void OnStateEnter()
    {
        // TODO:: winning check.
        // TODO:: children needs to call base.
    }

    /// <summary>
    /// Method is called on transitioning out of a state.
    /// </summary>
    public virtual void OnStateExit()
    {
        // Let children determine.
    }

    #region Helpers

    protected virtual bool PointerOnUI()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }

    protected virtual void Highlight(CTTUnit unit)
    {
        unit.MarkAsHighlighted();
    }

    protected virtual void Dehighlight(CTTUnit unit)
    {
        unit.UnMark();
    }

    protected virtual void Highlight(CTTCell cell)
    {
        // Mark as friend if the cell is of your faction.
        // Mark as enemy if the cell is occupied by an enemy.
        // Else, mark as normal.
        byte faction = m_gm.GetFaction(cell.Coord);
        byte currFac = m_gm.CurrentFaction;
        if (faction == currFac)
            cell.MarkAsFriendly();
        else if (CTTSFaction.IsFaction(faction))
            cell.MarkAsHostile();
        else
            cell.MarkAsHighlighted();
    }

    protected virtual void Dehighlight(CTTCell cell)
    {
        cell.UnMark();
    }

    #endregion
}
