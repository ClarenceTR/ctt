﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

class CTTGameStateUnitSelected : CTTGameState
{
    private CTTUnit m_unit;
    private CTTCell m_startpoint;
    private List<CTTUnit> m_vulnerableUnits;
    private List<CTTUnit> m_hiddenUnits;

    public CTTGameStateUnitSelected(CTTGameManager gameManager, CTTUnit unit) : base(gameManager)
    {
        m_unit = unit;
        m_vulnerableUnits = new List<CTTUnit>();
        m_hiddenUnits = new List<CTTUnit>();
    }

    public override byte GetFaction(Vector2Int position, in CTTSBoard originalBoard, in CTTSBoard mockBoard)
    {
        // Tell which faction it is from the mock board.
        return mockBoard[position].Faction;
    }

    public override void OnUnitHighlighted(CTTUnit unit)
    {
        // If mouse on a ui element, don't do anything.
        if (base.PointerOnUI()) return;

        this.Highlight(unit);
    }

    public override void OnUnitDehighlighted(CTTUnit unit)
    {
        this.Dehighlight(unit);
    }

    public override void OnUnitClicked(CTTUnit unit)
    {
        // If mouse on a ui element, don't do anything.
        if (base.PointerOnUI()) return;

        // If it's not the selected unit, I select that unit.
        if (unit != m_unit)
        {
            if (m_gm.CurrentPlayer.CanCommand(unit))
            {
                // TODO:: Deselect the current unit and select that unit, displaying available moves.
            }
        }
    }

    public override void OnCellHighlighted(CTTCell cell)
    {
        // If mouse on a ui element, don't do anything.
        if (base.PointerOnUI()) return;

        // If this cell has a unit on it, do the unit way.
        var unit = m_gm.GetUnitMock(cell.Coord);
        if (unit) { this.Highlight(unit); }
        else
        // If this cell is among the available moves, mark as green.
        // Else, mark as yellow.
        { this.Highlight(cell); }
    }

    public override void OnCellDehighlighted(CTTCell cell)
    {
        // If this cell has a unit on it, do the unit way.
        var unit = m_gm.GetUnitMock(cell.Coord);
        if (unit) { this.Dehighlight(unit); }
        else { this.Dehighlight(cell); }
    }

    public override void OnCellSelected(CTTCell cell)
    {
        // If mouse on a ui element, don't do anything.
        if (base.PointerOnUI()) return;

        // If this cell is among the available positions, mock move there.
        if (m_gm.IsAvailableMove(cell.Coord))
        {
            m_gm.MockMove(m_unit, cell.Coord);

            // Also, hide the captured units.
            HideVunerableUnits();

            // Call the event.
            m_gm.InvokeUnitMockMoved();
        }
        else
        {
            // Else if this cell has a unit on it, treat it as OnUnitclicked. 
            var unit = m_gm.GetUnitMock(cell.Coord);
            if (unit)
            { OnUnitClicked(unit); return; }
            else
            {
                // Else, just do nothing.

            }
        }
    }

    public override void OnActionUndone()
    {
        // If no steps to roll back, cancel.

        // Roll back one step.
    }

    public override void OnActionCancelled()
    {
        // Unselect anything.
        m_gm.DeselectUnit(m_unit);

        // Dehighlight the selected unit.
        base.Dehighlight(m_unit);
        base.Dehighlight(m_unit.Cell);

        // Go back to the waiting for input state.
        m_gm.GameState = new CTTGameStateWaitingForInput(m_gm);

        // Invoke event.
        m_gm.InvokeCancelledMockMove();

        UnmarkAllTotally();
    }

    public override void OnActionConfirmed()
    {
        // TODO:: Apply move.
        // Enter displaying stage.
        m_gm.EndTurn(m_unit);

        UnmarkAllTotally();
    }

    public override void OnStateEnter()
    {
        m_startpoint = m_unit.Cell;

        // This unit stays highlighted.
        m_unit.MarkAsHighlighted();

        // This cell stays start point.
        m_startpoint.MarkAsStartPoint();

        // Get the available cells this unit can go.
        m_gm.SelectUnit(m_unit);

        // Invoke event.
        m_gm.InvokeUnitSelected();
    }
    public override void OnStateExit()
    {
        // Unmark start point.
        m_startpoint.UnMark();

        // Unmark vunerable units.
        UnmarkVunerableUnits();

        // Blinking positions are unmarked in gm.EndTurn();
    }

    #region Helpers

    protected override void Highlight(CTTUnit unit)
    {
        // Mark as friend if the unit is of your faction.
        // Mark as enemy if the unit is occupied by an enemy.
        // Else, mark as normal.

        // If this unit is not the selected unit, 
        // Outline green if friendly.
        // Outline red if hostile.
        // Same to the cell.
        if (unit != m_unit)
        {
            byte faction = unit.Faction;
            byte currFac = m_gm.CurrentFaction;
            if (faction == currFac)
            {
                unit.MarkAsFriendly();
            }
            else if (CTTSFaction.IsFaction(faction))
            {
                unit.MarkAsHostile();
            }
            else
            {
                // This scenario normally won't happen.
                Debug.Assert(false);
            }
        }
        // Else, do nothing. The selected unit should stay highlighted.
    }
    protected override void Dehighlight(CTTUnit unit)
    {
        // If this unit is not the selected unit,
        // Unmark.
        if (unit != m_unit)
        {
            // If the unit is a hidden unit...
            if (m_vulnerableUnits.Contains(unit))
                unit.MarkAsVulnerable();
            else if (m_hiddenUnits.Contains(unit))
                unit.MarkAsHidden();
            else
                base.Dehighlight(unit);
        }

        // Else. do nothing. The selected unit should stay highlighted.
    }
    protected override void Highlight(CTTCell cell)
    {
        if (m_gm.IsAvailableMove(cell.Coord))
        {
            cell.MarkAsReachable();

            // If there are any units to be captured, mark them as vunerable.
            MarkVunerableUnits(cell);
        }
        else
            cell.MarkAsUnreachable();
    }
    protected override void Dehighlight(CTTCell cell)
    {
        if (cell.Coord == m_startpoint.Coord)
            cell.MarkAsStartPoint();
        else
            cell.UnMark();

        UnmarkVunerableUnits();
    }

    private void MarkVunerableUnits(in CTTCell targetCell)
    {
        // Precondition: targetCell should be amond the available positions.
        var move = m_gm.GetAvailableMove(targetCell.Coord);
        //if (move != null)
        Debug.Assert(move != null);
        {
            foreach (var pair in move.PiecesRemoved)
            {
                var pos = pair.Key;
                CTTSPiece piece = pair.Value;

                var unitVulnerable = m_gm.GetUnit(pos);
                /*if (unitVulnerable)*/
                Debug.Assert(unitVulnerable);
                {
                    m_vulnerableUnits.Add(unitVulnerable);
                    unitVulnerable.MarkAsVulnerable();
                }
            }
        }
    }

    private void UnmarkVunerableUnits()
    {
        foreach (var unitVulnerable in m_vulnerableUnits)
        { unitVulnerable.UnMarkTotally(); }
        m_vulnerableUnits.Clear();
    }

    private void HideVunerableUnits()
    {
        foreach (var unitVulnerable in m_vulnerableUnits)
        {
            unitVulnerable.UnMarkTotally();
            unitVulnerable.MarkAsHidden();
            m_hiddenUnits.Add(unitVulnerable);
        }
        m_vulnerableUnits.Clear();
    }

    private void UnmarkAllTotally()
    {
        foreach (var unit in m_vulnerableUnits)
        { unit.UnMarkTotally(); }
        foreach (var unit in m_hiddenUnits)
        { unit.UnMarkTotally(); }
    }

    //private List<Vector2Int> VulnerableOrHiddenPositions
    //{
    //    get
    //    {
    //        var res = new List<Vector2Int>();
    //        res.AddRange(m_vulnerableUnits.Select((u) => u.Position));
    //        res.AddRange(m_hiddenUnits.Select((u) => u.Position));
    //        return res;
    //    }
    //}

    #endregion
}
