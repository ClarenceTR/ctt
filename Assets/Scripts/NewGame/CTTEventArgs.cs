﻿using System;
using UnityEngine;

public class CTTUnitCreatedEventArgs : EventArgs
{
    public Transform unit;

    public CTTUnitCreatedEventArgs(Transform unit)
    {
        this.unit = unit;
    }
}