﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using UnityEditor;
using com.ootii.Cameras;
public class CTTGuiManager : MonoBehaviour
{
    CTTGameManager m_gm;
    CameraController mr_cm;
    //WorldRotationMotor mr_stupidMotor;
    CTTGuiPopupMenu mr_popup;
    [SerializeField] Canvas mr_canvas;

    public TextMeshProUGUI textInfo;
    public TextMeshProUGUI textRoundInfo;
    public LayerMask nodeLayer;
    public TextMeshProUGUI textCursor;
    public TextMeshProUGUI textState;
    public Image blue;
    public Image red;
    public Button btnOk;
    public Button btnCancel;

    public Color colorInactive = Color.cyan;
    public Color colorActiveBlue = Color.blue;
    public Color colorActiveRed = Color.red;

    void Awake()
    {
        m_gm = GetComponent<CTTGameManager>();
        mr_cm = Camera.main.GetComponentInParent<CameraController>();
        //mr_stupidMotor = mr_cm.ActiveMotor as WorldRotationMotor;
        mr_popup = FindObjectOfType<CTTGuiPopupMenu>();

        m_gm.GameStarted += OnGameStarted;
        m_gm.GameEnded += OnGameEnded;
        m_gm.TurnStarted += OnTurnStarted;
        m_gm.TurnEnded += OnTurnEnded;
        m_gm.UnitSelected += OnUnitSelected;
        m_gm.UnitMockMoved += OnUnitMockMoved;
        m_gm.CancelledMockMove += OnCancelledMockMove;
        m_gm.CantMockMoveAnyMore += OnCantMockMoveAnyMore;
        m_gm.UnitAdded += OnUnitAdded;
    }


    private void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, float.MaxValue, nodeLayer))
        {
            var cell = hit.collider.GetComponent<CTTCell>();
            textCursor.text = (cell.Coord).ToString();
        }

        var state = CTTGameManager.Instance?.GameState;
        textState.text = state?.GetType().ToString();
    }

    private void OnGameStarted(object sender, EventArgs e)
    {
        foreach (var cell in m_gm.Cells)
        {
            cell.CellHighlighted += OnCellHighlighted;
            cell.CellDehighlighted += OnCellDehighlighted;
        }

        // Turn off ui buttons.
        var cgOk = btnOk.GetComponent<CanvasGroup>(); cgOk.alpha = 0; cgOk.blocksRaycasts = false; cgOk.interactable = false;
        var cgCc = btnCancel.GetComponent<CanvasGroup>(); cgCc.alpha = 0; cgCc.blocksRaycasts = false; cgCc.interactable = false;

        OnTurnEnded(sender, e);
    }
    private void OnGameEnded(object sender, EventArgs e)
    {
        byte faction = (sender as CTTGameManager).CurrentPlayer.Faction;
        textInfo.text = (faction == CTTSFaction.Two ? "Red" : "Blue") + " wins!";
    }
    private void OnTurnStarted(object sender, EventArgs e)
    {
        textRoundInfo.text = $"Round #{m_gm.CurrentRound}";

        byte faction = (sender as CTTGameManager).CurrentPlayer.Faction;
        DisplayCurrent(faction);
    }
    private void OnTurnEnded(object sender, EventArgs e)
    {
        //HideOkCc();
        mr_popup.Hide();
        //NextTurnButton.interactable = ((sender as CTTGameManager).CurrentPlayer is HumanPlayer);
        //int cpn = (sender as CTTGameManager).CurrentPlayerNumber;

        //textCurrentPlayer.text = "Player " + (cpn + 1) + $", {CTTSLogic.GetFaction(cpn)}";
        //textCurrentPlayer.text = $"Current player: {((faction == CTTSFaction.Two) ? "Red" : "Blue")}";
    }
    private void OnUnitSelected(object sender, EventArgs e)
    {
        //ShowOkCc();
    }
    private void OnUnitMockMoved(object sender, EventArgs e)
    {
        // Show pop up.
        mr_popup.Show(mr_canvas);
    }
    private void OnCancelledMockMove(object sender, EventArgs e)
    {
        //HideOkCc();

        // Hide pop up.
        mr_popup.Hide();
    }

    private void ShowOkCc()
    {
        var cgOk = btnOk.GetComponent<CanvasGroup>();    /*   cgOk.alpha = 0; cgOk.blocksRaycasts = false; cgOk.interactable = false;*/
        var cgCc = btnCancel.GetComponent<CanvasGroup>();/* cgCc.alpha = 0; cgCc.blocksRaycasts = false; cgCc.interactable = false;*/
        cgOk.DOFade(1, 1).OnComplete(delegate ()
        {
            cgOk.blocksRaycasts = true; cgOk.interactable = true;
        });
        cgCc.DOFade(1, 1).OnComplete(delegate ()
        {
            cgCc.blocksRaycasts = true; cgCc.interactable = true;
        });
    }

    private void HideOkCc()
    {
        var cgOk = btnOk.GetComponent<CanvasGroup>();
        var cgCc = btnCancel.GetComponent<CanvasGroup>();
        cgOk.DOFade(0, 1).OnComplete(delegate ()
        {
            cgOk.blocksRaycasts = false; cgOk.interactable = false;
        });
        cgCc.DOFade(0, 1).OnComplete(delegate ()
        {
            cgCc.blocksRaycasts = false; cgCc.interactable = false;
        });
    }

    private void OnCantMockMoveAnyMore(object sender, EventArgs e)
    {
    }

    private void OnUnitHighlighted(object sender, EventArgs e)
    {
        var unit = sender as CTTUnit;
        textInfo.text = $"{CTTSType.ToName(unit.Type)}, {unit.Faction.ToString()}";
    }
    private void OnUnitDehighlighted(object sender, EventArgs e)
    {

    }
    private void OnCellHighlighted(object sender, EventArgs e)
    {
        var cell = sender as CTTCell;
        var piece = m_gm.GetPieceMock(cell.Coord);
        if (CTTSFaction.IsFaction(piece.Faction))
        { textInfo.text = $"{CTTSType.ToName(piece.Type)}, {piece.Faction.ToString()}"; }
        else if (piece.Type == CTTSType.Null) { textInfo.text = "Out of Bound"; }
        else { textInfo.text = $"{CTTSType.ToName(piece.Type)}"; }
    }
    private void OnCellDehighlighted(object sender, EventArgs e)
    {
    }

    private void OnUnitAttacked(object sender, AttackEventArgs e)
    {
        //if (!(m_gm.CurrentPlayer is HumanPlayer)) return;
        //OnUnitDehighlighted(sender, new EventArgs());

        //if ((sender as Unit).HitPoints <= 0) return;

        //OnUnitHighlighted(sender, e);
    }
    private void OnUnitAdded(object sender, CTTUnitCreatedEventArgs e)
    {
        RegisterUnit(e.unit);
    }

    private void RegisterUnit(Transform unit)
    {
        unit.GetComponent<CTTUnit>().UnitHighlighted += OnUnitHighlighted;
        unit.GetComponent<CTTUnit>().UnitDehighlighted += OnUnitDehighlighted;
        unit.GetComponent<CTTUnit>().UnitAttacked += OnUnitAttacked;
    }
    public void RestartLevel()
    {
        //Application.LoadLevel(Application.loadedLevel);
    }

    private void DisplayCurrent(byte faction)
    {
        if (faction == CTTSFaction.One)
        {
            blue.color = colorActiveBlue;
            red.color = colorInactive;
        }
        else
        {
            blue.color = colorInactive;
            red.color = colorActiveRed;
        }
    }

}
