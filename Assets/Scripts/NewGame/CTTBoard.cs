﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains all the borad infomation.
/// </summary>
public class CTTBoard
{
    Dictionary<Vector2Int, CTTCell> m_board;

    public CTTCell this[Vector2Int position]
    {
        get
        {
            // If this position is an available location, return the slot.
            // Else, return null.
            if (m_board.ContainsKey(position))
                return m_board[position];
            else
                return null;
        }
        set
        {
            var piece = this[position];
            if (!piece)
            {
                m_board[position] = value;
            }
            // If out of bound, nothing happens.
        }
    }

    public CTTCell this[int x, int y]
    {
        get
        {
            var v = new Vector2Int(x, y);
            return this[v];
        }
        set
        {
            var v = new Vector2Int(x, y);
            this[v] = value;
        }
    }

    public IEnumerable<CTTCell> Cells=>m_board.Values;

    public CTTBoard()
    {
        m_board = new Dictionary<Vector2Int, CTTCell>();
    }

    public void Reset(IEnumerable<CTTCell> cells)
    {
        m_board.Clear();
        foreach(var cell in cells)
        {
            m_board.Add(cell.Coord, cell);
        }
    }

}

