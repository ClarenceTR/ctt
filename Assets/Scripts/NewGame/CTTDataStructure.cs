﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CTTPositionInfo
{
    public Cell cell = null;
}

public class CTTTurnResult
{
    public byte winner = CTTSFaction.None;
    public byte loser = CTTSFaction.None;

    public bool HasAWinner => winner != CTTSFaction.None;
    public bool HasALoser => loser!= CTTSFaction.None;
}

public static class CTTPieceOrientation
{
    public static Quaternion Right => Quaternion.Euler(0, 90, 0);
    public static Quaternion Upright => Quaternion.Euler(0, 30, 0);
    public static Quaternion Upleft => Quaternion.Euler(0, -30, 0);
    public static Quaternion Left => Quaternion.Euler(0, -90, 0);
    public static Quaternion Downleft => Quaternion.Euler(0, -150, 0);
    public static Quaternion Downright => Quaternion.Euler(0, -210, 0);

    public static Quaternion GetQuaternion(CTTSDirection direction)
    {
        switch (direction)
        {
            case CTTSDirection.Right:
                return Right;
            case CTTSDirection.Upright:
                return Upright;
            case CTTSDirection.Upleft:
                return Upleft;
            case CTTSDirection.Left:
                return Left;
            case CTTSDirection.Downleft:
                return Downleft;
            case CTTSDirection.Downright:
                return Downright;
            case CTTSDirection.Invalid:
            default:
                return Quaternion.identity;
        }
    }
}
