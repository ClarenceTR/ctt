﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using DG.Tweening;

public class CTTGuiButton : MonoBehaviour,
    IPointerEnterHandler,
    IPointerExitHandler,
    IPointerDownHandler,
    IPointerUpHandler
{
    float enterScale = 1.2f;
    float downScale = 0.8f;
    float normalScale = 1f;

    RectTransform rectTransform;

    public void OnPointerDown(PointerEventData eventData)
    {
        rectTransform.DOScale(downScale, 0.2f);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        rectTransform.DOScale(enterScale, 0.2f);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        rectTransform.DOScale(normalScale, 0.2f);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        rectTransform.DOScale(enterScale, 0.2f);
        Clicked?.Invoke();
    }

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public UnityEvent Clicked;
}
