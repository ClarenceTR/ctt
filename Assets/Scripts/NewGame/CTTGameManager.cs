﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CTTGameManager : Singleton<CTTGameManager>
{
    #region Events

    /// <summary>
    /// LevelLoading event is invoked before Initialize method is run.
    /// </summary>
    public event EventHandler LevelLoading;
    /// <summary>
    /// LevelLoadingDone event is invoked after Initialize method has finished running.
    /// </summary>
    public event EventHandler LevelLoadingDone;
    /// <summary>
    /// GameStarted event is invoked at the beggining of StartGame method.
    /// </summary>
    public event EventHandler GameStarted;
    /// <summary>
    /// GameEnded event is invoked when there is a single player left in the game.
    /// </summary>
    public event EventHandler GameEnded;
    /// <summary>
    /// Turn ended event is invoked at the end of each turn.
    /// </summary>
    public event EventHandler TurnEnded;
    public event EventHandler TurnStarted;
    /// <summary>
    /// UnitAdded event is invoked each time AddUnit method is called.
    /// </summary>
    public event EventHandler<CTTUnitCreatedEventArgs> UnitAdded;

    public event EventHandler UnitSelected; public void InvokeUnitSelected() { UnitSelected?.Invoke(this, new EventArgs()); }
    public event EventHandler UnitMockMoved; public void InvokeUnitMockMoved() { UnitMockMoved?.Invoke(this, new EventArgs()); }
    //public event EventHandler StartedMockMove; public void InvokeStartedMockMove() { StartedMockMove?.Invoke(this, new EventArgs()); }
    public event EventHandler CancelledMockMove; public void InvokeCancelledMockMove() { CancelledMockMove?.Invoke(this, new EventArgs()); }
    public event EventHandler CantMockMoveAnyMore; public void InvokeCantMockMoveAnyMore() { CantMockMoveAnyMore?.Invoke(this, new EventArgs()); }

    // public void Invoke() { ?.Invoke(this, new EventArgs()); }

    public event EventHandler ActionUndone;
    public event EventHandler ActionCancelled; public void CancelMove() { ActionCancelled?.Invoke(this, new EventArgs()); }
    public event EventHandler ActionConfirmed; public void ConfirmMove() { ActionConfirmed?.Invoke(this, new EventArgs()); }

    #endregion

    [SerializeField] Transform m_unitsParent;
    CTTGridGenerator mr_gridGenerator;

    CTTUnitFactory m_unitFactory;

    CTTGameState m_gameState; //The grid delegates some of its behaviours to cellGridState object.
    CTTBoard m_board;
    List<CTTPlayer> m_players;
    List<CTTUnit> m_units;

    CTTSBoard ms_board;
    List<CTTSPlayer> ms_players;
    int m_currPlNum = 0;
    int m_currRndNum = 1;
    CTTTurnResult m_currTurnResult = new CTTTurnResult();
    Dictionary<Vector2Int, CTTSAvailableMove> m_currentAvailableMoves;

    public CTTGameState GameState
    {
        get
        {
            return m_gameState;
        }
        set
        {
            if (m_gameState != null)
                m_gameState.OnStateExit();
            m_gameState = value;
            m_gameState.OnStateEnter();
        }
    }

    public Dictionary<Vector2Int, CTTSAvailableMove> CurrentAvailableMoves { get => m_currentAvailableMoves; set => m_currentAvailableMoves = value; }

    public IEnumerable<CTTCell> Cells => m_board.Cells;

    public CTTPlayer CurrentPlayer => m_players[m_currPlNum];
    public byte CurrentFaction => ms_players[m_currPlNum].Faction;
    public int NumberOfPlayers => m_players.Count;
    public int CurrentRound => m_currRndNum;

    public CTTPlayer GetPlayer(byte faction)
    {
        var player = m_players.Find((p) => p.Faction == faction);
        return player;
    }

    public byte GetFaction(Vector2Int position)
    {
        return m_gameState.GetFaction(position, ms_board, CurrentLogicPlayer.MockBoard);
    }

    public CTTSPiece GetPiece(Vector2Int position)
    {
        return ms_board[position];
    }

    public CTTSPiece GetPieceMock(Vector2Int position)
    {
        var clp = CurrentLogicPlayer;
        if (!clp.Selected) return GetPiece(position);
        return clp.MockBoard[position];
    }

    public CTTUnit GetUnit(Vector2Int position)
    {
        return m_units.Find((u) => u.Position == position);
    }

    public CTTUnit GetUnitMock(Vector2Int mockPosition)
    {
        return m_units.Find((u) => u.MockPosition == mockPosition);
    }

    public CTTSAvailableMove GetAvailableMove(Vector2Int position)
    {
        if (m_currentAvailableMoves.ContainsKey(position))
            return m_currentAvailableMoves[position];
        return null;
    }

    public bool Selected => CurrentLogicPlayer.Selected;
    public Vector2Int SelectedPosition => CurrentLogicPlayer.SelectedPosition;

    public bool IsAvailableMove(Vector2Int position) => m_currentAvailableMoves.ContainsKey(position);

    public Dictionary<Vector2Int, CTTSAvailableMove> SelectUnit(in CTTUnit unit)
    {
        m_currentAvailableMoves.Clear();

        // First select.
        var p = CurrentLogicPlayer;
        if (p.Select(unit.Position))
        {
            // Then return the calculated available moves.
            m_currentAvailableMoves = p.CalculateAvailable();

            //// Set mock position. Just in case.
            //unit.MockCell = unit.Cell;

            // Display these moves.
            D_MarkMovablePositions();
        }

        return m_currentAvailableMoves;
    }

    public void DeselectUnit(in CTTUnit selectedUnit)
    {
        var p = CurrentLogicPlayer;
        p.SelectNothing();

        // Unmark the available moves.
        D_UnMarkPositionTotally();

        // Put this selected unit back.
        selectedUnit.PlaceAt(selectedUnit.Cell);

        m_currentAvailableMoves.Clear();
    }

    public void MockMove(CTTUnit unit, Vector2Int toPosition)
    {
        var p = CurrentLogicPlayer;
        Debug.Assert(p.Selected);
        Debug.Assert(p.SelectedPosition == unit.MockPosition);

        p.MockMove(toPosition);

        // Before updating the current available moves array, unmark them.
        D_UnMarkPositionTotally();

        // Do the new available position calculation.
        m_currentAvailableMoves = p.CalculateAvailable();

        // Display piece move.
        D_MockMoveUnit(unit, toPosition);

        // Display new available positions.
        D_MarkMovablePositions();

        // If there are no available position, invoke an event.
        if (m_currentAvailableMoves.Count == 0) InvokeCantMockMoveAnyMore();
    }

    public Stack<CTTSAvailableMove> MoveStack => new Stack<CTTSAvailableMove>(CurrentLogicPlayer.MoveStack.Reverse());

    public void ApplyMove(in CTTSAvailableMove move, in CTTUnit unit)
    {
        // Go to the end position.
        // Logic:
        ms_board[move.EndPosition] = ms_board[move.StartPosition];
        ms_board[move.StartPosition] = CTTSPiece.Empty;

        // If the piece is a prince and reaches the capital, wins.
        if (unit.Type == CTTSType.Prince && move.EndPosition.Equals(new Vector2Int(-6, 12)))
        {
            m_currTurnResult.winner = unit.Faction;
        }

        // Display:
        D_MoveUnit(unit, move.EndPosition, move.MoveType);

        // Remove the pieces on the real board, as well as the display board.
        foreach (var pair in move.PiecesRemoved)
        {
            var pos = pair.Key;
            CTTSPiece piece = pair.Value;

            // If the removed piece is a prince, the player loses.
            if (piece.Type == CTTSType.Prince)
            {
                m_currTurnResult.loser = piece.Faction;
                // Currently, the game is 2-player. so one loses the other wins.
                if (piece.Faction == CTTSFaction.One) { m_currTurnResult.winner = CTTSFaction.Two; }
                else if (piece.Faction == CTTSFaction.Two) { m_currTurnResult.winner = CTTSFaction.One; }
                else { /* The piece is neither One or Two. Game goes on.*/ }
            }

            // Logic:
            ms_board[pos] = CTTSPiece.Empty;
            var unitToRemove = GetUnit(pos);
            if (unitToRemove)
            {
                // Clean references to this unit.
                m_units.Remove(unitToRemove);
                GetPlayer(unitToRemove.Faction).RemoveUnit(unitToRemove);

                // Display:
                unitToRemove.MoveOffBoard();
            }
        }
    }

    private void D_MarkMovablePositions()
    {
        foreach (var position in m_currentAvailableMoves.Keys)
        {
            var cell = m_board[position];
            cell?.MarkAsIndicated();
        }
    }

    private void D_UnMarkPositionTotally()
    {
        foreach (var position in m_currentAvailableMoves.Keys)
        {
            var cell = m_board[position];
            cell?.UnMarkTotally();
        }

    }

    private void D_MarkUnitBlink()
    {

    }

    //private void D_ShowMock(in CTTSPlayer player, CTTSAvailableMove move)
    //{
    //    Debug.Assert(player.Selected);

    //    var moveStack = player.MoveStack;
    //    if (moveStack.Count == 0) return;

    //    var unit = GetUnit(move.StartPosition);

    //    D_MockMoveUnit(unit, move.EndPosition);

    //    foreach (var pieceToRemove in move.PiecesRemoved)
    //    {
    //        D_HideUnit(unit);
    //    }
    //}

    private void D_MockMoveUnit(CTTUnit unit, Vector2Int position)
    {
        CTTCell targetCell = m_board[position];
        Debug.Assert(targetCell != null);

        unit.MockMoveTo(targetCell);
    }

    private void D_MoveUnit(CTTUnit unit, Vector2Int position, byte moveType)
    {
        CTTCell targetCell = m_board[position];
        Debug.Assert(targetCell != null);

        unit.MoveTo(targetCell, moveType);
    }

    private void D_PlaceUnit(CTTUnit unit, Vector2Int position)
    {
        CTTCell targetCell = m_board[position];
        Debug.Assert(targetCell != null);

        unit.PlaceAt(targetCell);
    }

    private void D_HideUnit(CTTUnit unit)
    {

    }

    private void Start()
    {
        LevelLoading?.Invoke(this, new EventArgs());

        Initialize();

        if (LevelLoadingDone != null)
            LevelLoadingDone.Invoke(this, new EventArgs());

        StartGame();
    }

    private void Initialize()
    {
        mr_gridGenerator = GetComponent<CTTGridGenerator>();
        m_unitFactory = GetComponent<CTTUnitFactory>();
        m_currentAvailableMoves = new Dictionary<Vector2Int, CTTSAvailableMove>();

        // Logic board.
        ms_board = new CTTSBoard(); ms_board.Reset();

        // Game board.
        // Read generated cells from the scene.
        var cells = transform.GetComponentsInChildren<CTTCell>();
        // Add to the cell information.
        m_board = new CTTBoard(); m_board.Reset(cells);
        // Register cell events.
        RegisterCellEvents(ref cells);

        // Logic players.
        ms_players = new List<CTTSPlayer>()
        {
            new CTTSPlayer(ms_board, CTTSFaction.One),
            new CTTSPlayer(ms_board, CTTSFaction.Two),
        };

        // Game players.
        m_players = new List<CTTPlayer>()
        {
            new CTTHumanPlayer(CTTSFaction.One),
            new CTTHumanPlayer(CTTSFaction.Two),
        };

        // (Logic units have been initialized during ms_board.Reset())
        // Game units. 
        m_units = new List<CTTUnit>();
        GenerateUnitsFromSimulationBoard(ref m_units);
    }

    private CTTSPlayer CurrentLogicPlayer => ms_players[m_currPlNum];

    private void GenerateUnitsFromSimulationBoard(ref List<CTTUnit> units)
    {
        // New the units list.
        units = new List<CTTUnit>();

        // Clear everything under UnitsParent.
        foreach (Transform child in m_unitsParent)
        {
            Destroy(child.gameObject);
        }

        foreach (var pair in ms_board.Board)
        {
            var pos = pair.Key;
            var piece = pair.Value;

            // Display any position occupied.
            var prefab = m_unitFactory.GetUnit(piece.Type, piece.Faction);
            if (prefab != null)
            {
                // Get the cell.
                var cell = m_board[pos];

                // Instantiate this unit.
                CTTSDirection orientation = CTTLogic.DetermineOrientation(piece.Faction, pos);
                Quaternion quaternion = CTTPieceOrientation.GetQuaternion(orientation);
                var unit = Instantiate
                    (
                        prefab,
                        new Vector3(cell.transform.position.x, 0, cell.transform.position.z),
                        quaternion,
                        m_unitsParent
                    ).GetComponent<CTTUnit>();
                unit.Info = piece;
                unit.MockCell = unit.Cell = cell;
                unit.Orientation = orientation;

                // Add to the units list.
                units.Add(unit);

                // Add this unit to players' unit ref list.
                var pl = GetPlayer(piece.Faction);
                pl?.AddUnit(unit);

                // Register unit events.
                RegisterUnitEvents(unit);

                // Register game events.
                RegisterGameEvents();
            }
        }
    }

    private void RegisterCellEvents(ref CTTCell[] cells)
    {
        foreach (var cell in cells)
        {
            cell.CellClicked += OnCellClicked;
            cell.CellHighlighted += OnCellHighlighted;
            cell.CellDehighlighted += OnCellDehighlighted;
        }
    }

    /// <summary>
    /// Adds unit to the game
    /// </summary>
    /// <param name="unit">Unit to add</param>
    private void RegisterUnitEvents(CTTUnit unit)
    {
        unit.UnitHighlighted += OnUnitHighlighted;
        unit.UnitDehighlighted += OnUnitDehighlighted;
        unit.UnitClicked += OnUnitClicked;
        unit.UnitDestroyed += OnUnitDestroyed;

        UnitAdded?.Invoke(this, new CTTUnitCreatedEventArgs(unit.transform));
    }

    private void RegisterGameEvents()
    {
        ActionUndone += OnActionUndone;
        ActionCancelled += OnActionCancelled;
        ActionConfirmed += OnActionConfirmed;
    }

    /// <summary>
    /// Method is called once, at the beggining of the game.
    /// </summary>
    public void StartGame()
    {
        // Invoke GameStarted event.
        GameStarted?.Invoke(this, new EventArgs());

        //m_units.FindAll(u => CurrentPlayer.CanCommand(u)).ForEach(u => { u.OnTurnStart(); });
        //foreach (var u in CurrentPlayer.Units) u.OnTurnStart();
        CurrentPlayer.Play(this);

        // Invoke TurnStarted event.
        TurnStarted?.Invoke(this, new EventArgs());
    }
    /// <summary>
    /// Method makes turn transitions. It is called by player at the end of his turn.
    /// Ends the current turn and enters the displaying move state.
    /// </summary>
    public void EndTurn(CTTUnit selectedUnit)
    {
        // Unmark any blinking positions.
        D_UnMarkPositionTotally();

        GameState = new CTTGameStateTurnChanging(this, selectedUnit);

        // Start a new record of CTTTurnResult for ApplyMove() to write.
        m_currTurnResult = new CTTTurnResult();

        //m_units.FindAll(u => CurrentPlayer.CanCommand(u)).ForEach(u => { u.OnTurnEnd(); });
        //// Invoke units' turn end event.
        //foreach (var u in CurrentPlayer.Units) u.OnTurnEnd();

        // Invoke TurnEnded event.
        TurnEnded?.Invoke(this, new EventArgs());
    }

    public void NextTurn()
    {
        // Check if the game ends.
        if (m_currTurnResult.HasAWinner)
        {
            // Game ends.
            GameEnded.Invoke(this, new EventArgs());

            GameState = new CTTGameStateGameOver(this);
        }
        else
        {
            // Increase the round number.
            if (m_currPlNum + 1 >= NumberOfPlayers) m_currRndNum += 1;

            // Go to the next player.
            m_currPlNum = (m_currPlNum + 1) % NumberOfPlayers;

            // Don't need to skip the players that are defeated.

            // Initialize the new player for this turn.
            //foreach (var u in CurrentPlayer.Units) u.OnTurnStart();
            CurrentPlayer.Play(this);

            // Invoke TurnStarted event.
            TurnStarted?.Invoke(this, new EventArgs());
        }
    }

    public void Reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }

    #region EventHandlers

    private void OnCellDehighlighted(object sender, EventArgs e)
    {
        m_gameState.OnCellDehighlighted(sender as CTTCell);
    }
    private void OnCellHighlighted(object sender, EventArgs e)
    {
        m_gameState.OnCellHighlighted(sender as CTTCell);
    }
    private void OnCellClicked(object sender, EventArgs e)
    {
        m_gameState.OnCellSelected(sender as CTTCell);
    }

    private void OnUnitHighlighted(object sender, EventArgs e)
    {
        m_gameState.OnUnitHighlighted(sender as CTTUnit);
    }
    private void OnUnitDehighlighted(object sender, EventArgs e)
    {
        m_gameState.OnUnitDehighlighted(sender as CTTUnit);
    }
    private void OnUnitClicked(object sender, EventArgs e)
    {
        m_gameState.OnUnitClicked(sender as CTTUnit);
    }
    private void OnUnitDestroyed(object sender, AttackEventArgs e)
    {
        m_units.Remove(sender as CTTUnit);
    }

    private void OnActionUndone(object sender, EventArgs e)
    {
        m_gameState.OnActionUndone();
    }
    private void OnActionCancelled(object sender, EventArgs e)
    {
        m_gameState.OnActionCancelled();
    }
    private void OnActionConfirmed(object sender, EventArgs e)
    {
        m_gameState.OnActionConfirmed();
    }
    #endregion

    #region Input Detection

    private void Update()
    {
        //if(Input.GetMouseButtonUp(1))
        //{
        //    // Right click rolls back the action.
        //    ActionUndone?.Invoke(this, new EventArgs());
        //}

        if (Input.GetMouseButtonUp(1) ||
            Input.GetKeyUp(KeyCode.Escape))
        {
            // Escape cancells the action.
            ActionCancelled?.Invoke(this, new EventArgs());
        }
    }

    #endregion
}
