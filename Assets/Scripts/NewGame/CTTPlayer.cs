﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class CTTPlayer
{
    protected byte m_faction;
    protected List<CTTUnit> m_units;

    public byte Faction => m_faction;
    public IEnumerable<CTTUnit> Units => m_units;

    public CTTPlayer()
    {
        m_units = new List<CTTUnit>();
    }

    public virtual void AddUnit(CTTUnit unit)
    {
        m_units.Add(unit);
    }

    public virtual void RemoveUnit(CTTUnit unit)
    {
        m_units.Remove(unit);
    }

    /// <summary>
    /// Method is called every turn. Allows player to interact with his units.
    /// </summary>         
    public abstract void Play(CTTGameManager gameManager);

    public virtual bool CanCommand(CTTUnit unit)
    {
        return unit.Faction == m_faction;
    }
}
