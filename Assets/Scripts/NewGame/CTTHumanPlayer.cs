﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class CTTHumanPlayer : CTTPlayer
{
    public CTTHumanPlayer(byte faction) : base()
    {
        m_faction = faction;
    }

    public override void Play(CTTGameManager gameManager)
    {
        gameManager.GameState = new CTTGameStateWaitingForInput(gameManager);
    }
}
