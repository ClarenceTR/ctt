﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTTUnit : MonoBehaviour
{
    private CTTCell mr_cell;
    private CTTCell mr_mockCell;
    private CTTUnitManager mr_manager;

    private byte m_faction;
    private Renderer m_renderer;
    private Material m_defaultMaterial;

    public CTTSPiece Info { protected get; set; }
    public CTTCell Cell { get => mr_cell; set => mr_cell = value; }
    public CTTCell MockCell { get => mr_mockCell; set => mr_mockCell = value; }
    public CTTSDirection Orientation { get; set; }

    public byte Faction { get => Info.Faction; }
    public byte Type => Info.Type;
    public Vector2Int Position => mr_cell.Coord;
    public Vector2Int MockPosition => mr_mockCell.Coord;

    /// <summary>
    /// Indicates if movement animation is playing.
    /// </summary>
    public bool IsMoving { get; set; }

    private void Start()
    {
        m_renderer = GetComponentInChildren<Renderer>();
        m_defaultMaterial = m_renderer.material;
        mr_manager = GetComponentInParent<CTTUnitManager>();
    }

    public void MockMoveTo(in CTTCell targetCell)
    {
        // TODO:: The code happens to be the same as PlaceAt. Shouldn't be. Will have animation.
        var hexSize = Cell.GetCellDimensions();
        Vector3 newPos = CTTLogic.LogicToViewPosition(targetCell.Coord.x, targetCell.Coord.y, ref hexSize);

        // Move the unit.
        transform.position = newPos;
        MockCell = targetCell;
    }

    public void MoveTo(in CTTCell targetCell, byte moveType)
    {
        float jh = (moveType == CTTSMoveType.Step) ? 5f : 30f;
        StartCoroutine(C_MoveTo(targetCell, jh, .5f, 1f));
    }

    IEnumerator C_MoveTo(CTTCell targetCell, float jumpHeight, float turnDuration, float jumpDuration)
    {
        IsMoving = true;

        var mp = Position; var tp = targetCell.Coord;

        // First turn then jump.
        var direction = CTTSLogic.GetDirection(ref mp, ref tp);
        if (direction != Orientation)
        {
            // Turn.
            yield return C_D_TurnTo(direction, turnDuration);
        }

        // Move.
        yield return C_D_JumpTo(targetCell, jumpHeight, turnDuration);

        // Set logic properties.
        Orientation = direction;
        MockCell = Cell = targetCell;

        IsMoving = false;
    }

    IEnumerator C_D_TurnTo(CTTSDirection direction, float duration)
    {
        Quaternion originalQuaternion = transform.rotation;
        Quaternion targetQuaternion = CTTPieceOrientation.GetQuaternion(direction);

        float lerpTime = 0;
        float perc = 0;
        while (perc < 1)
        {
            lerpTime += Time.deltaTime;
            perc = lerpTime / duration;
            // Do thing.
            transform.rotation = Quaternion.Lerp(originalQuaternion, targetQuaternion, perc);

            yield return new WaitForEndOfFrame();
        }
        transform.rotation = targetQuaternion;
    }

    IEnumerator C_D_JumpTo(CTTCell targetCell, float jumpHeight, float duration)
    {
        var hexSize = Cell.GetCellDimensions();
        Vector3 oriPos = transform.position;
        Vector3 newPos = CTTLogic.LogicToViewPosition(targetCell.Coord.x, targetCell.Coord.y, ref hexSize);

        // It jumps. y goes according to the curve.
        // x and z goes the direction portion.
        float lerpTime = 0;
        float perc = 0;
        while (perc < 1)
        {
            lerpTime += Time.deltaTime;
            perc = lerpTime / duration;
            // Do thing.
            float y = jumpHeight * mr_manager.JumpCurve.Evaluate(perc);
            float x = Mathf.Lerp(oriPos.x, newPos.x, perc);
            float z = Mathf.Lerp(oriPos.z, newPos.z, perc);
            transform.position = new Vector3(x, y, z);
            yield return new WaitForEndOfFrame();
        }
    }

    bool BeingDestroyed { get; set; } = false;
    public void MoveOffBoard()
    {
        // Fade out and destroy.
        StartCoroutine(C_MoveOffBoard(0.7f, 1.2f));
    }

    private IEnumerator C_MoveOffBoard(float delay, float duration)
    {
        BeingDestroyed = true;
        yield return new WaitForSeconds(delay);
        yield return C_D_MoveOffBoard(duration);
        Destroy(gameObject);
    }

    private IEnumerator C_D_MoveOffBoard(float duration)
    {
        m_renderer.material = mr_manager.matRemovalFade;
        var c = m_renderer.material.color;
        float aFrom = 1, aTo = 0;
        float waitingTime = 0.1f;

        float lerpTime = 0;
        float perc = 0;
        while (perc < 1)
        {
            lerpTime += waitingTime;
            perc = lerpTime / duration;
            // Do thing.
            // Maybe Unmark() changes the material back in the middle. So check.
            if (m_renderer.material != mr_manager.matRemovalFade) m_renderer.material = mr_manager.matRemovalFade;
            var a = Mathf.Lerp(aFrom, aTo, mr_manager.BlinkCurve.Evaluate(perc));
            m_renderer.material.color = new Color(c.r, c.g, c.b, a);
            yield return new WaitForSeconds(waitingTime);
        }
    }

    public void MockPlaceAt(in CTTCell targetCell)
    {
        var hexSize = Cell.GetCellDimensions();
        Vector3 newPos = CTTLogic.LogicToViewPosition(targetCell.Coord.x, targetCell.Coord.y, ref hexSize);
        transform.position = newPos;
        MockCell = targetCell;
    }

    public void PlaceAt(in CTTCell targetCell)
    {
        var hexSize = Cell.GetCellDimensions();
        Vector3 newPos = CTTLogic.LogicToViewPosition(targetCell.Coord.x, targetCell.Coord.y, ref hexSize);
        transform.position = newPos;
        MockCell = Cell = targetCell;
    }

    #region Events

    /// <summary>
    /// UnitClicked event is invoked when user clicks the unit. 
    /// It requires a collider on the unit game object to work.
    /// </summary>
    public event EventHandler UnitClicked;
    /// <summary>
    /// UnitSelected event is invoked when user clicks on unit that belongs to him. 
    /// It requires a collider on the unit game object to work.
    /// </summary>
    public event EventHandler UnitSelected;
    /// <summary>
    /// UnitDeselected event is invoked when user click outside of currently selected unit's collider.
    /// It requires a collider on the unit game object to work.
    /// </summary>
    public event EventHandler UnitDeselected;
    /// <summary>
    /// UnitHighlighted event is invoked when user moves cursor over the unit. 
    /// It requires a collider on the unit game object to work.
    /// </summary>
    public event EventHandler UnitHighlighted;
    /// <summary>
    /// UnitDehighlighted event is invoked when cursor exits unit's collider. 
    /// It requires a collider on the unit game object to work.
    /// </summary>
    public event EventHandler UnitDehighlighted;
    /// <summary>
    /// UnitAttacked event is invoked when the unit is attacked.
    /// </summary>
    public event EventHandler<AttackEventArgs> UnitAttacked;
    /// <summary>
    /// UnitDestroyed event is invoked when unit's hitpoints drop below 0.
    /// </summary>
    public event EventHandler<AttackEventArgs> UnitDestroyed;
    /// <summary>
    /// UnitMoved event is invoked when unit moves from one cell to another.
    /// </summary>
    public event EventHandler<MovementEventArgs> UnitMoved;

    #endregion

    #region Input Detection

    protected virtual void OnMouseDown()
    {
        if (UnitClicked != null)
            UnitClicked.Invoke(this, new EventArgs());
    }
    protected virtual void OnMouseEnter()
    {
        if (UnitHighlighted != null)
            UnitHighlighted.Invoke(this, new EventArgs());
    }
    protected virtual void OnMouseExit()
    {
        if (UnitDehighlighted != null)
            UnitDehighlighted.Invoke(this, new EventArgs());
    }

    #endregion

    #region Event Handlers

    /// <summary>
    /// Method is called at the start of each turn.
    /// </summary>
    public void OnTurnStart()
    {
        //MovementPoints = TotalMovementPoints;
        //ActionPoints = TotalActionPoints;

        //SetState(new UnitStateMarkedAsFriendly(this));
    }
    /// <summary>
    /// Method is called at the end of each turn.
    /// </summary>
    public void OnTurnEnd()
    {
        //cachedPaths = null;
        //Buffs.FindAll(b => b.Duration == 0).ForEach(b => { b.Undo(this); });
        //Buffs.RemoveAll(b => b.Duration == 0);
        //Buffs.ForEach(b => { b.Duration--; });

        //SetState(new UnitStateNormal(this));
    }
    /// <summary>
    /// Method is called when units HP drops below 1.
    /// </summary>
    protected void OnDestroyed()
    {
        //Cell.IsTaken = false;
        //MarkAsDestroyed();
        //Destroy(gameObject);
    }

    /// <summary>
    /// Method is called when unit is selected.
    /// </summary>
    public void OnUnitSelected()
    {
        UnitSelected?.Invoke(this, new EventArgs());
    }
    /// <summary>
    /// Method is called when unit is deselected.
    /// </summary>
    public void OnUnitDeselected()
    {
        UnitDeselected?.Invoke(this, new EventArgs());
    }

    #endregion

    /// <summary>
    /// Gives visual indication that the unit is eatable.
    /// </summary>
    public void MarkAsVulnerable()
    {
        m_renderer.material = mr_manager.matTrans;

        if (c_blink != null) StopCoroutine(c_blink);
        c_blink = StartCoroutine(C_BlinkTranslucent(1, 0, 1f));
    }

    public void MarkAsHighlighted()
    {
        // Just highlight the outline.
        m_renderer.material = mr_manager.matHighlighted;
        Color c = mr_manager.colorNormal;
        m_renderer.material.SetVector("_OutlineColor", new Vector4(c.r, c.g, c.b, c.a));

        // Highlight the cell.
        MockCell.MarkAsHighlighted();
    }

    public void MarkAsFriendly()
    {
        m_renderer.material = mr_manager.matHighlighted;
        Color c = mr_manager.colorFriendly;
        m_renderer.material.SetVector("_OutlineColor", new Vector4(c.r, c.g, c.b, c.a));

        MockCell.MarkAsFriendly();
    }

    public void MarkAsHostile()
    {
        m_renderer.material = mr_manager.matHighlighted;
        Color c = mr_manager.colorHostile;
        m_renderer.material.SetVector("_OutlineColor", new Vector4(c.r, c.g, c.b, c.a));

        MockCell.MarkAsHostile();
    }

    internal void MarkAsHidden()
    {
        m_renderer.material = mr_manager.matTrans;

        var c = m_renderer.material.color;
        c.a = 0;
        m_renderer.material.color = c;
        MockCell.UnMark();
    }

    public void MarkAsSelected()
    {
        // TODO:: 

        if (c_blink != null) StopCoroutine(c_blink);
        c_blink = StartCoroutine(C_BlinkHighlighted(178f / 255f, 20f / 255f, 1.5f));
    }

    public void UnMark()
    {
        m_renderer.material = m_defaultMaterial;

        MockCell.UnMark();
    }

    public void UnMarkTotally()
    {
        UnMark();

        // Also Cancel the blinking.
        if (c_blink != null) StopCoroutine(c_blink);

        MockCell.UnMarkTotally();
    }

    private Coroutine c_blink = null;
    private IEnumerator C_BlinkTranslucent(float a1, float a2, float duration)
    {
        var c = m_renderer.material.color;
        float waitingTime = 0.1f;
        while (true)
        {
            float lerpTime = 0;
            while (true)
            {
                lerpTime += waitingTime;
                float perc = lerpTime / duration;
                if (perc > 1) break;
                var a = Mathf.Lerp(a1, a2, mr_manager.BlinkCurve.Evaluate(perc));
                m_renderer.material.color = new Color(c.r, c.g, c.b, a);
                yield return new WaitForSeconds(waitingTime);
            }
            lerpTime = 0;
            while (true)
            {
                lerpTime += waitingTime;
                float perc = lerpTime / duration;
                if (perc > 1) break;
                var a = Mathf.Lerp(a2, a1, mr_manager.BlinkCurve.Evaluate(perc));
                m_renderer.material.color = new Color(c.r, c.g, c.b, a);
                yield return new WaitForSeconds(waitingTime);
            }
        }

    }
    private IEnumerator C_BlinkHighlighted(float a1, float a2, float duration)
    {
        var c = mr_manager.matHighlighted.GetVector("_Color");
        float waitingTime = 0.1f;
        while (true)
        {
            float lerpTime = 0;
            while (true)
            {
                lerpTime += waitingTime;
                float perc = lerpTime / duration;
                if (perc > 1) break;
                var a = Mathf.Lerp(a1, a2, mr_manager.BlinkCurve.Evaluate(perc));
                m_renderer.material.SetVector("_Color", new Vector4(c.x, c.y, c.z, a));
                yield return new WaitForSeconds(waitingTime);
            }
            lerpTime = 0;
            while (true)
            {
                lerpTime += waitingTime;
                float perc = lerpTime / duration;
                if (perc > 1) break;
                var a = Mathf.Lerp(a2, a1, mr_manager.BlinkCurve.Evaluate(perc));
                m_renderer.material.SetVector("_Color", new Vector4(c.x, c.y, c.z, a));
                yield return new WaitForSeconds(waitingTime);
            }
        }
    }


}