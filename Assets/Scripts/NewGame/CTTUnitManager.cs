﻿using System;
using System.Collections.Generic;
using UnityEngine;

class CTTUnitManager : Singleton<CTTUnitManager>
{
    [SerializeField] AnimationCurve m_blinkCurve;
    [SerializeField] AnimationCurve m_jumpCurve;

    [Header("Materials")]
    public Material matHighlighted;
    public Material matTrans;
    public Material matRemovalFade;
    //public Material matHighlightedPawn;
    //public Material matHighlightedKnight;
    //public Material matHighlightedRook;
    //public Material matHighlightedPrince;
    //public Material matTransPawn;
    //public Material matTransKnight;
    //public Material matTransRook;
    //public Material matTransPrince;

    [Header("Colors")]
    public Color colorNormal;
    public Color colorFriendly;
    public Color colorHostile;

    public AnimationCurve BlinkCurve => m_blinkCurve;
    public AnimationCurve JumpCurve => m_jumpCurve;
}
