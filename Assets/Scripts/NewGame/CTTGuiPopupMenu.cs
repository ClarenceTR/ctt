﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CTTGuiPopupMenu : MonoBehaviour
{
    CanvasGroup m_canvasGroup;
    bool m_shown = false;

    public bool Shown
    {
        get => m_shown;
        set
        {
            m_shown = value;
            if (m_canvasGroup)
            {
                m_canvasGroup.alpha = value ? 1 : 0;
                m_canvasGroup.interactable = value;
                m_canvasGroup.blocksRaycasts = value;
            }
        }
    }
    void Start()
    {
        m_canvasGroup = GetComponent<CanvasGroup>();
        Shown = false;
    }

    public void Show(Canvas canvas)
    {
        // Get to the cursor position.
        //Vector2 lp;
        //var uiPos = RectTransformUtility.ScreenPointToLocalPointInRectangle
        //    (
        //        canvas.GetComponent<RectTransform>(),
        //        Input.mousePosition,
        //        Camera.main,
        //        out lp);
        //var uiPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        var vpPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        var centeredBasedVPPOS = vpPos - new Vector3(0.5f, 0.5f);
        var canvasRect = canvas.GetComponent<RectTransform>();
        var scale = canvasRect.sizeDelta;
        transform.localPosition = Vector3.Scale(centeredBasedVPPOS, scale);

        Shown = true;
    }

    public void Hide()
    {
        Shown = false;
    }
}
