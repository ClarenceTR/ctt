﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class CTTCellManager : Singleton<CTTCellManager>
{
    [SerializeField]
    AnimationCurve m_blinkCurve;

    public AnimationCurve BlinkCurve => m_blinkCurve;
}
