﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
class CTTGameStateTurnChanging : CTTGameState
{
    private CTTUnit m_unit;
    private Stack<CTTSAvailableMove> m_moveStack;

    public CTTGameStateTurnChanging(CTTGameManager gameManager, CTTUnit unit) : base(gameManager)
    {
        m_unit = unit;
    }

    public override void OnStateEnter()
    {
        // Waiting. No input is gonna work.

        // Get move stack.
        m_moveStack = m_gm.MoveStack;

        // Apply the moves and display.
        m_gm.StartCoroutine(C_ApplyAndDisplay());
    }

    public override void OnStateExit()
    {
        // Dehighlight the selected unit.
        base.Dehighlight(m_unit);
    }

    IEnumerator C_ApplyAndDisplay(float interval = 1f)
    {
        m_unit.PlaceAt(m_unit.Cell);
        //yield return new WaitForSeconds(interval);

        while (m_moveStack.Count > 0)
        {
            // Display a move.
            var move = m_moveStack.Pop();
            m_gm.ApplyMove(move, m_unit);

            // Wait.
            //yield return new WaitForSeconds(interval * 1f);
            yield return new WaitUntil(() => m_unit.IsMoving == false);
            yield return new WaitForSeconds(1f);
        }
        // Display Over.
        // End turn.
        m_gm.NextTurn();
    }
}
