﻿using System.Collections.Generic;
using UnityEngine;
class CTTGameStateWaitingForInput : CTTGameState
{
    Dictionary<Vector2Int, CTTSAvailableMove> m_currentAvailableMoves;

    public CTTGameStateWaitingForInput(CTTGameManager gameManager) : base(gameManager)
    {
    }

    // I don't override GetFaction();

    public override void OnUnitHighlighted(CTTUnit unit)
    {
        this.Highlight(unit);
    }

    // I don't override OnUnitDehighlighted();

    public override void OnUnitClicked(CTTUnit unit)
    {
        // If it's my unit, I can command. Enter the selection mode.
        if (m_gm.CurrentPlayer.CanCommand(unit))
            m_gm.GameState = new CTTGameStateUnitSelected(m_gm, unit);

        // TODO:: Else, play an error sound showing you can't choose this unit.
    }

    public override void OnCellHighlighted(CTTCell cell)
    {
        // If this cell has a unit on it, do the unit way.
        var unit = m_gm.GetUnit(cell.Coord);
        if (unit) { this.Highlight(unit); }
        else
        // If this cell is among the available moves, mark as green.
        // Else, mark as yellow.
        { this.Highlight(cell); }
    }

    // I don't override OnCellDehighlighted();

    public override void OnCellSelected(CTTCell cell)
    {
        // Does this cell contain a unit?
        var unit = m_gm.GetUnit(cell.Coord);
        if (unit) if (m_gm.CurrentPlayer.CanCommand(unit))
            m_gm.GameState = new CTTGameStateUnitSelected(m_gm, unit);
        // If doesn't have a unit, do nothing.
    }

    // I don't override OnActionUndone();

    public override void OnActionCancelled()
    {
        // I do nothing when cancel.
    }

    // I don't override OnActionConfirmed();

    // I don't override OnStateEnter();

    // I don't override OnStateExit();

    #region Helpers

    protected override void Highlight(CTTUnit unit)
    {
        byte faction = unit.Faction;
        byte currFac = m_gm.CurrentFaction;
        if (faction == currFac)
        {
            unit.MarkAsHighlighted();
        }
        else if (CTTSFaction.IsFaction(faction))
        {
            unit.MarkAsHostile();
        }
        else
        {
            // This scenario normally won't happen.
            Debug.Assert(false);
        }

    }

    protected override void Highlight(CTTCell cell)
    {
        base.Highlight(cell);
    }

    #endregion
}
