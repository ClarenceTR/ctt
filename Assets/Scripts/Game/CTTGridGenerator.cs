﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode()]
public class CTTGridGenerator : MonoBehaviour
{
    [Header("Important Logic")]
    public Transform CellsParent;
    public GameObject CellPrefab;
    public int Radius = 14;

    [Header("Materials")]
    public Material matNormalTerritory;
    public Material matFacOneTerritory;
    public Material matFacTwoTerritory;
    public Material matCaptalTerritory;
    public Material matMarginTerritory;
    public Material matNormalOutline;
    public Material matMarginOutline;

    CTTCell CreateCell(int x, int y, Material territory = null, Material outline = null)
    {
        CTTCell cell = Instantiate(CellPrefab).GetComponent<CTTCell>();
        var hexSize = cell.GetCellDimensions();
        cell.Coord = new Vector2Int(x, y);
        cell.transform.position = CTTLogic.LogicToViewPosition(x, y, ref hexSize);
        cell.transform.parent = CellsParent;

        // Paint
        if (territory != null && outline != null)
        {
            var renderers = cell.GetComponentsInChildren<Renderer>();
            if (renderers.Length == 2)
            {
                renderers[0].material = territory;
                renderers[1].material = outline;
            }
        }

        return cell;
    }

    public List<CTTCell> GenerateGrid()
    {
        var hexagons = new List<CTTCell>();
        hexagons.Clear();

        // Create a hexagon of radius 8.
        int b = 8 - 1;
        for (int x = -b; x <= b; x++)
        {
            for (int y = Mathf.Max(-x - b, -b); y <= Mathf.Min(-x + b, b); y++)
            {
                hexagons.Add(CreateCell(x, y));
            }
        }

        // Add capital triangle:
        for (int x = -6; x <= -2; x++)
            for (int y = 8; y <= -x + 6; y++)
            {
                hexagons.Add(CreateCell(x, y, matCaptalTerritory, matNormalOutline));
            }

        // Add the y=-x faction (Right):
        for (int x = -10; x <= -8; x++)
            for (int y = -x - 5; y <= 5; y++)
            {
                // The supporter's area for right
                hexagons.Add(CreateCell(x, y, matFacTwoTerritory, matNormalOutline));

                // The fief for right
                hexagons.Add(CreateCell(-x, -y, matFacTwoTerritory, matNormalOutline));
            }

        // Add the y = x faction:
        for (int x = 3; x <= 5; x++)
            for (int y = -x + 8; y <= 5; y++)
            {
                // The supporter's area for left
                hexagons.Add(CreateCell(x, y, matFacOneTerritory, matNormalOutline));

                // The fief for left
                hexagons.Add(CreateCell(-x, -y, matFacOneTerritory, matNormalOutline));
            }

        // Add margin cells:
        {
            int y = -8;
            Material mt = matMarginTerritory, mo = matMarginOutline;

            for (int x = 0; x <= 8; x++) hexagons.Add(CreateCell(x, y, mt, mo));
            y = -7;
            hexagons.Add(CreateCell(-1, y, mt, mo));
            hexagons.Add(CreateCell(8, y, mt, mo));
            y = -6;
            for (int x = -5; x <= -2; x++) hexagons.Add(CreateCell(x, y, mt, mo));
            for (int x = 8; x <= 11; x++) hexagons.Add(CreateCell(x, y, mt, mo));
            y = -5;
            hexagons.Add(CreateCell(-6, y, mt, mo));
            hexagons.Add(CreateCell(11, y, mt, mo));
            y = -4;
            hexagons.Add(CreateCell(-6, y, mt, mo));
            hexagons.Add(CreateCell(10, y, mt, mo));
            y = -3;
            hexagons.Add(CreateCell(-6, y, mt, mo));
            hexagons.Add(CreateCell(9, y, mt, mo));
            y = -2;
            hexagons.Add(CreateCell(-6, y, mt, mo));
            hexagons.Add(CreateCell(8, y, mt, mo));
            y = -1;
            hexagons.Add(CreateCell(-7, y, mt, mo));
            hexagons.Add(CreateCell(8, y, mt, mo));
            y = 0;
            hexagons.Add(CreateCell(-8, y, mt, mo));
            hexagons.Add(CreateCell(8, y, mt, mo));
            y = 1;
            hexagons.Add(CreateCell(-8, y, mt, mo));
            hexagons.Add(CreateCell(7, y, mt, mo));
            y = 2;
            hexagons.Add(CreateCell(-8, y, mt, mo));
            hexagons.Add(CreateCell(6, y, mt, mo));
            y = 3;
            hexagons.Add(CreateCell(-9, y, mt, mo));
            hexagons.Add(CreateCell(6, y, mt, mo));
            y = 4;
            hexagons.Add(CreateCell(-10, y, mt, mo));
            hexagons.Add(CreateCell(6, y, mt, mo));
            y = 5;
            hexagons.Add(CreateCell(-11, y, mt, mo));
            hexagons.Add(CreateCell(6, y, mt, mo));
            y = 6;
            for (int x = -11; x <= -8; x++) hexagons.Add(CreateCell(x, y, mt, mo));
            for (int x = 2; x <= 5; x++) hexagons.Add(CreateCell(x, y, mt, mo));
            y = 7;
            hexagons.Add(CreateCell(-8, y, mt, mo));
            hexagons.Add(CreateCell(1, y, mt, mo));
            y = 8;
            hexagons.Add(CreateCell(-8, y, mt, mo));
            hexagons.Add(CreateCell(-7, y, mt, mo));
            hexagons.Add(CreateCell(-1, y, mt, mo));
            hexagons.Add(CreateCell(0, y, mt, mo));
            y = 9;
            hexagons.Add(CreateCell(-7, y, mt, mo));
            hexagons.Add(CreateCell(-2, y, mt, mo));
            y = 10;
            hexagons.Add(CreateCell(-7, y, mt, mo));
            hexagons.Add(CreateCell(-3, y, mt, mo));
            y = 11;
            hexagons.Add(CreateCell(-7, y, mt, mo));
            hexagons.Add(CreateCell(-4, y, mt, mo));
            y = 12;
            hexagons.Add(CreateCell(-7, y, mt, mo));
            hexagons.Add(CreateCell(-5, y, mt, mo));
            y = 13;
            hexagons.Add(CreateCell(-7, y, mt, mo));
            hexagons.Add(CreateCell(-6, y, mt, mo));
        }
        return hexagons;
    }
}
