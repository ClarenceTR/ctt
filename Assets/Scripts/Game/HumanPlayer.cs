/// <summary>
/// Class representing a human player.
/// </summary>
public class HumanPlayer : Player
{
    private void Start()
    {
    }

    public override void Play(CTTGameManagerOld cellGrid)
    {
        cellGrid.GameState = new CTTGameStateWaitingForInputOld(cellGrid);
    }
}