﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTTCellOld : Hexagon
{
    private Renderer hexagonRenderer;
    private Renderer outlineRenderer;
    private Color defaultHexagonColor;
    private Color defaultOutlineColor;

    public void Awake()
    {
        hexagonRenderer = GetComponent<Renderer>();

        var outline = transform.Find("Outline");
        outlineRenderer = outline.GetComponent<Renderer>();

        defaultHexagonColor = GetColor(hexagonRenderer);
        defaultOutlineColor = GetColor(outlineRenderer);
        SetColor(hexagonRenderer, defaultHexagonColor);
        SetColor(outlineRenderer, defaultOutlineColor);
    }

    public override Vector3 GetCellDimensions()
    {
        var outline = transform.Find("Outline");
        var outlineRenderer = outline.GetComponent<Renderer>();
        var size = outlineRenderer.bounds.size;
        return size;
    }

    public override void MarkAsReachable()
    {
        SetColor(hexagonRenderer, Color.yellow);
    }
    public override void MarkAsPath()
    {
        SetColor(hexagonRenderer, Color.cyan);
    }
    public override void MarkAsHighlighted()
    {
        SetColor(hexagonRenderer, Color.blue); ;
        SetColor(outlineRenderer, Color.yellow);
    }
    public override void UnMark()
    {
        SetColor(hexagonRenderer, defaultHexagonColor);
        SetColor(outlineRenderer, defaultOutlineColor);
    }

    private void SetColor(Renderer renderer, Color color)
    {
        renderer.material.color = color;
    }

    private Color GetColor(Renderer renderer)
    {
        return renderer.material.color;
    }

    public void SetColorOutline(Color color)
    {
        SetColor(outlineRenderer, color);
    }

    public void SetColorCell(Color color)
    {
        SetColor(hexagonRenderer, color);
    }
}
