﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTTUnitOld : Unit
{
    public CTTSPiece Info { get; set; }
    public CTTCellOld CTTCell;

    public Vector2Int Position => new Vector2Int((int)Cell.OffsetCoord.x, (int)Cell.OffsetCoord.y);

    public override bool IsCellMovableTo(Cell cell)
    {
        return true;
    }

    public override void MarkAsAttacking(Unit other)
    {
    }

    public override void MarkAsDefending(Unit other)
    {
    }

    public override void MarkAsDestroyed()
    {
    }

    public override void MarkAsFinished()
    {
    }

    public override void MarkAsFriendly()
    {
    }

    public override void MarkAsReachableEnemy()
    {
    }

    public override void MarkAsSelected()
    {
    }

    public override void UnMark()
    {
    }
}
