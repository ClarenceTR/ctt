﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class represents a game participant.
/// </summary>
public abstract class Player : MonoBehaviour
{
    public int PlayerNumber;

    public byte Faction => CTTLogic.GetFaction(PlayerNumber);

    public List<Unit> Troops = new List<Unit>();

    /// <summary>
    /// Method is called every turn. Allows player to interact with his units.
    /// </summary>         
    public abstract void Play(CTTGameManagerOld cellGrid);
}