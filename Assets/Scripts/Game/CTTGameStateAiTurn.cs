public class CTTGameStateAiTurn : CTTGameStateOld
{
    public CTTGameStateAiTurn(CTTGameManagerOld cellGrid) : base(cellGrid)
    {      
    }

    public override void OnStateEnter()
    {
        base.OnStateEnter();
        foreach (var cell in m_gm.Cells)
        {
            cell.UnMark();
        }
    }
}