﻿class CTTGameStateWaitingForInputOld : CTTGameStateOld
{
    public CTTGameStateWaitingForInputOld(CTTGameManagerOld cellGrid) : base(cellGrid)
    {
    }

    public override void OnUnitClicked(Unit unit)
    {
        if(unit.PlayerNumber.Equals(m_gm.CurrentPlayerNumber))
            m_gm.GameState = new CTTGameStateUnitSelectedOld(m_gm, unit); 
    }
}
