﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

/// <summary>
/// CellGrid class keeps track of the game, stores cells, units and players objects. It starts the game and makes turn transitions. 
/// It reacts to user interacting with units or cells, and raises events related to game progress. 
/// </summary>
public class CTTGameManagerOld : Singleton<CTTGameManagerOld>
{
    /// <summary>
    /// LevelLoading event is invoked before Initialize method is run.
    /// </summary>
    public event EventHandler LevelLoading;
    /// <summary>
    /// LevelLoadingDone event is invoked after Initialize method has finished running.
    /// </summary>
    public event EventHandler LevelLoadingDone;
    /// <summary>
    /// GameStarted event is invoked at the beggining of StartGame method.
    /// </summary>
    public event EventHandler GameStarted;
    /// <summary>
    /// GameEnded event is invoked when there is a single player left in the game.
    /// </summary>
    public event EventHandler GameEnded;
    /// <summary>
    /// Turn ended event is invoked at the end of each turn.
    /// </summary>
    public event EventHandler TurnEnded;

    /// <summary>
    /// UnitAdded event is invoked each time AddUnit method is called.
    /// </summary>
    public event EventHandler<UnitCreatedEventArgsOld> UnitAdded;

    private CTTGameStateOld m_GameState; //The grid delegates some of its behaviours to cellGridState object.
    public CTTGameStateOld GameState
    {
        get
        {
            return m_GameState;
        }
        set
        {
            if (m_GameState != null)
                m_GameState.OnStateExit();
            m_GameState = value;
            m_GameState.OnStateEnter();
        }
    }

    public int NumberOfPlayers { get; private set; }

    public CTTSPlayer CurrentPlayerInSim => ms_players[CurrentPlayerNumber];

    public Player CurrentPlayer
    {
        get { return Players.Find(p => p.PlayerNumber.Equals(CurrentPlayerNumber)); }
    }
    public int CurrentPlayerNumber { get; private set; }

    public Player GetPlayer(byte faction)
    {
        return Players.Find(p => p.Faction == faction);
    }

    /// <summary>
    /// GameObject that holds player objects.
    /// </summary>
    public Transform PlayersParent;
    public Transform UnitsParent;
    public Transform CellsParent;

    public List<Player> Players { get; private set; }
    public List<Cell> Cells { get; private set; }
    public List<Unit> Units { get; private set; }

    private void Start()
    {
        if (LevelLoading != null)
            LevelLoading.Invoke(this, new EventArgs());

        Initialize();

        if (LevelLoadingDone != null)
            LevelLoadingDone.Invoke(this, new EventArgs());

        StartGame();
    }

    CTTSBoard ms_board;
    CTTSPlayer[] ms_players;

    CTTUnitFactory m_pieceFactory;
    public Dictionary<Vector2Int, CTTPositionInfo> PositionInfo { get; private set; }

    public bool Ready = false;

    private void InitializeLogic()
    {
        // Set up board.
        ms_board = new CTTSBoard(); ms_board.Reset();

        // Set up necessary game components.
        m_pieceFactory = GetComponent<CTTUnitFactory>();
        PositionInfo = new Dictionary<Vector2Int, CTTPositionInfo>();

        // Set up players.
        ms_players = new CTTSPlayer[]
        {
            new CTTSPlayer(ms_board, CTTSFaction.One),
            new CTTSPlayer(ms_board, CTTSFaction.Two),
        };
    }

    private void Initialize()
    {
        // Initialize logic.
        InitializeLogic();

        // Initialize scene.
        Players = new List<Player>();
        for (int i = 0; i < PlayersParent.childCount; i++)
        {
            var player = PlayersParent.GetChild(i).GetComponent<Player>();
            if (player != null)
                Players.Add(player);
            else
                Debug.LogError("Invalid object in Players Parent game object");
        }
        NumberOfPlayers = Players.Count;
        CurrentPlayerNumber = Players.Min(p => p.PlayerNumber);

        Cells = new List<Cell>();
        var cells = transform.GetComponentsInChildren<Cell>();

        //for (int i = 0; i < childCount; i++)
        foreach (var cell in cells)
        {
            //var cell = transform.GetChild(i).gameObject.GetComponent<Cell>();
            if (cell != null)
            {
                // Add to the cells list,
                Cells.Add(cell);

                // Also add reference to the position info map.
                PositionInfo.Add
                    (
                        new Vector2Int((int)cell.OffsetCoord.x, (int)cell.OffsetCoord.y),
                        new CTTPositionInfo()
                        {
                            cell = cell,
                        }
                    );
            }
            else
                Debug.LogError("Invalid object in cells paretn game object");
        }

        foreach (var cell in Cells)
        {
            cell.CellClicked += OnCellClicked;
            cell.CellHighlighted += OnCellHighlighted;
            cell.CellDehighlighted += OnCellDehighlighted;
            cell.GetComponent<Cell>().GetNeighbours(Cells);
        }

        // Units.
        GenerateUnitsFromSimulationBoard();

        Ready = true;
    }

    private void GenerateUnitsFromSimulationBoard()
    {
        // New the units list.
        Units = new List<Unit>();

        // Clear everything under UnitsParent.
        foreach (Transform child in UnitsParent)
        {
            Destroy(child.gameObject);
        }

        foreach (var pair in ms_board.Board)
        {
            var pos = pair.Key;
            var piece = pair.Value;

            // Display any position occupied.
            var prefab = m_pieceFactory.GetUnit(piece.Type, piece.Faction);
            if (prefab != null)
            {
                // Get the cell.
                var cell = PositionInfo[pos].cell;

                // Instantiate this unit.
                CTTSDirection orientation = CTTLogic.DetermineOrientation(piece.Faction, pos);
                Quaternion quaternion = CTTPieceOrientation.GetQuaternion(orientation);
                var unit = Instantiate
                    (
                        prefab,
                        new Vector3(cell.transform.position.x, 0, cell.transform.position.z),
                        quaternion,
                        UnitsParent
                    ).GetComponent<CTTUnitOld>();
                cell.IsTaken = true;
                unit.Cell = cell;
                unit.CTTCell = cell as CTTCellOld;
                unit.PlayerNumber = GetPlayer(piece.Faction).PlayerNumber;
                unit.Info = piece;
                unit.Initialize();

                // Add to the units list.
                Units.Add(unit);

                // Add to the event list.
                AddUnit(unit.transform);

                // Belong to the player of this faction.
                var p = GetPlayer(piece.Faction);
                p.Troops.Add(unit.GetComponent<Unit>());
            }
        }
    }

    private void OnCellDehighlighted(object sender, EventArgs e)
    {
        GameState.OnCellDeselected(sender as Cell);
    }
    private void OnCellHighlighted(object sender, EventArgs e)
    {
        GameState.OnCellSelected(sender as Cell);
    }
    private void OnCellClicked(object sender, EventArgs e)
    {
        GameState.OnCellClicked(sender as Cell);
    }

    private void OnUnitClicked(object sender, EventArgs e)
    {
        GameState.OnUnitClicked(sender as Unit);
    }
    private void OnUnitDestroyed(object sender, AttackEventArgs e)
    {
        Units.Remove(sender as Unit);
        var totalPlayersAlive = Units.Select(u => u.PlayerNumber).Distinct().ToList(); //Checking if the game is over
        if (totalPlayersAlive.Count == 1)
        {
            if (GameEnded != null)
                GameEnded.Invoke(this, new EventArgs());
        }
    }

    /// <summary>
    /// Adds unit to the game
    /// </summary>
    /// <param name="unit">Unit to add</param>
    public void AddUnit(Transform unit)
    {
        unit.GetComponent<Unit>().UnitClicked += OnUnitClicked;
        unit.GetComponent<Unit>().UnitDestroyed += OnUnitDestroyed;

        if (UnitAdded != null)
            UnitAdded.Invoke(this, new UnitCreatedEventArgsOld(unit));
    }

    /// <summary>
    /// Method is called once, at the beggining of the game.
    /// </summary>
    public void StartGame()
    {
        if (GameStarted != null)
            GameStarted.Invoke(this, new EventArgs());

        Units.FindAll(u => u.PlayerNumber.Equals(CurrentPlayerNumber)).ForEach(u => { u.OnTurnStart(); });
        Players.Find(p => p.PlayerNumber.Equals(CurrentPlayerNumber)).Play(this);
    }
    /// <summary>
    /// Method makes turn transitions. It is called by player at the end of his turn.
    /// </summary>
    public void EndTurn()
    {
        if (Units.Select(u => u.PlayerNumber).Distinct().Count() == 1)
        {
            return;
        }
        GameState = new CTTGameStateTurnChangingOld(this);

        Units.FindAll(u => u.PlayerNumber.Equals(CurrentPlayerNumber)).ForEach(u => { u.OnTurnEnd(); });

        CurrentPlayerNumber = (CurrentPlayerNumber + 1) % NumberOfPlayers;
        while (Units.FindAll(u => u.PlayerNumber.Equals(CurrentPlayerNumber)).Count == 0)
        {
            CurrentPlayerNumber = (CurrentPlayerNumber + 1) % NumberOfPlayers;
        }//Skipping players that are defeated.

        if (TurnEnded != null)
            TurnEnded.Invoke(this, new EventArgs());

        Units.FindAll(u => u.PlayerNumber.Equals(CurrentPlayerNumber)).ForEach(u => { u.OnTurnStart(); });
        Players.Find(p => p.PlayerNumber.Equals(CurrentPlayerNumber)).Play(this);
    }
}
