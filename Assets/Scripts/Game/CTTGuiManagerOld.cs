﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CTTGuiManagerOld : MonoBehaviour
{
    CTTGameManagerOld m_gm;

    public TextMeshProUGUI textCurrentPlayer;
    public TextMeshProUGUI textInfo;

    void Awake()
    {
        m_gm = GetComponent<CTTGameManagerOld>();


        m_gm.GameStarted += OnGameStarted;
        m_gm.TurnEnded += OnTurnEnded;
        m_gm.GameEnded += OnGameEnded;
        m_gm.UnitAdded += OnUnitAdded;
    }

    private void OnGameStarted(object sender, EventArgs e)
    {
        foreach (var cell in m_gm.Cells)
        {
            cell.CellHighlighted += OnCellHighlighted;
            cell.CellDehighlighted += OnCellDehighlighted;
        }

        OnTurnEnded(sender, e);
    }

    private void OnGameEnded(object sender, EventArgs e)
    {
        textInfo.text = "Player " + ((sender as CTTGameManagerOld).CurrentPlayerNumber + 1) + " wins!";
    }
    private void OnTurnEnded(object sender, EventArgs e)
    {
        //NextTurnButton.interactable = ((sender as CTTGameManager).CurrentPlayer is HumanPlayer);
        int cpn = (sender as CTTGameManagerOld).CurrentPlayerNumber;
        textCurrentPlayer.text = "Player " + (cpn + 1)+ $", {CTTLogic.GetFaction(cpn)}";
    }
    private void OnCellDehighlighted(object sender, EventArgs e)
    {
        
    }
    private void OnCellHighlighted(object sender, EventArgs e)
    {
    }
    private void OnUnitAttacked(object sender, AttackEventArgs e)
    {
        if (!(m_gm.CurrentPlayer is HumanPlayer)) return;
        OnUnitDehighlighted(sender, new EventArgs());

        if ((sender as Unit).HitPoints <= 0) return;

        OnUnitHighlighted(sender, e);
    }
    private void OnUnitDehighlighted(object sender, EventArgs e)
    {
       
    }
    private void OnUnitHighlighted(object sender, EventArgs e)
    {
        var unit = sender as CTTUnitOld;
        textInfo.text = $"{CTTSType.ToName(unit.Info.Type)}, {unit.Info.Faction.ToString()}";
    }
    private void OnUnitAdded(object sender, UnitCreatedEventArgsOld e)
    {
        RegisterUnit(e.unit);
    }

    private void RegisterUnit(Transform unit)
    {
        unit.GetComponent<Unit>().UnitHighlighted += OnUnitHighlighted;
        unit.GetComponent<Unit>().UnitDehighlighted += OnUnitDehighlighted;
        unit.GetComponent<Unit>().UnitAttacked += OnUnitAttacked;
    }
    public void RestartLevel()
    {
        //Application.LoadLevel(Application.loadedLevel);
    }
}
