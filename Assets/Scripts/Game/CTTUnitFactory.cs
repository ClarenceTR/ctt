﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTTUnitFactory : Singleton<CTTUnitFactory>
{
    public GameObject pf_pawn_blue;
    public GameObject pf_knight_blue;
    public GameObject pf_rook_blue;
    public GameObject pf_prince_blue;
    public GameObject pf_pawn_red;
    public GameObject pf_knight_red;
    public GameObject pf_rook_red;
    public GameObject pf_prince_red;

    public GameObject GetUnit(byte type, byte faction)
    {
        switch (type)
        {
            case CTTSType.Pawn:
                return Pawn(faction);
            case CTTSType.Knight:
                return Knight(faction);
            case CTTSType.Rook:
                return Rook(faction);
            case CTTSType.Prince:
                return Prince(faction);
            default:
                return null;
        }
    }

    public GameObject Rook(byte faction)
    {
        if (faction == CTTSFaction.One)
            return pf_rook_blue;
        return pf_rook_red;
    }

    public GameObject Pawn(byte faction)
    {
        if (faction == CTTSFaction.One)
            return pf_pawn_blue;
        return pf_pawn_red;
    }

    public GameObject Knight(byte faction)
    {
        if (faction == CTTSFaction.One)
            return pf_knight_blue;
        return pf_knight_red;
    }

    public GameObject Prince(byte faction)
    {
        if (faction == CTTSFaction.One)
            return pf_prince_blue;
        return pf_prince_red;
    }
}
