﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CTTUnitGenerator))]
public class CTTUnitGeneratorHelper : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        CTTUnitGenerator unitGenerator = (CTTUnitGenerator)target;

        if (GUILayout.Button("Snap to Grid"))
        {
            unitGenerator.SnapToGrid();
        }
    }
}
